<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\WilayahController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\SumateraController;
use App\Http\Controllers\JawaController;
use App\Http\Controllers\KalimantanController;
use App\Http\Controllers\BaliLombokController;
use App\Http\Controllers\SulawesiController;
use App\Http\Controllers\PulauController;
use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::get('/', [HomeController::class, 'index']);


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/admin', [DashboardController::class, 'index']);
});

require __DIR__ . '/auth.php';

Route::resource('kategori', KategoriController::class)->names([
    'index' => 'kategori.index',
    'create' => 'kategori.create',
    'store' => 'kategori.store',
    'show' => 'kategori.show',
    'edit' => 'kategori.edit',
    'update' => 'kategori.update',
    'destroy' => 'kategori.destroy',
]);

Route::resource('produk', ProdukController::class)->names([
    'index' => 'produk.index',
    'create' => 'produk.create',
    'store' => 'produk.store',
    'show' => 'produk.show',
    'edit' => 'produk.edit',
    'update' => 'produk.update',
    'destroy' => 'produk.destroy',
]);


Route::resource('wilayah', WilayahController::class)->names([
    'index' => 'wilayah.index',
    'create' => 'wilayah.create',
    'store' => 'wilayah.store',
    'show' => 'wilayah.show',
    'edit' => 'wilayah.edit',
    'update' => 'wilayah.update',
    'destroy' => 'wilayah.destroy',
]);

Route::resource('pulau', PulauController::class)->names([
    'index' => 'pulau.index',
    'create' => 'pulau.create',
    'store' => 'pulau.store',
    'show' => 'pulau.show',
    'edit' => 'pulau.edit',
    'update' => 'pulau.update',
    'destroy' => 'pulau.destroy',
]);




Route::get('/session', [SessionController::class, 'index']);
Route::post('/session/login', [SessionController::class, 'login']);
Route::post('/session/logout', [SessionController::class, 'logout']);

//Sulawesi
//Makasar
Route::get('/Makasar', [SulawesiController::class, 'Makasar']);
//Palu
Route::get('/Palu', [SulawesiController::class, 'Palu']);
//Kendari
Route::get('/Kendari', [SulawesiController::class, 'Kendari']);
//Gorantalo
Route::get('/Gorontalo', [SulawesiController::class, 'Gorontalo']);
//Manado
Route::get('/Manado', [SulawesiController::class, 'Manado']);

//Kalimantan
//Pontianak
Route::get('/Pontianak', [KalimantanController::class, 'Pontianak']);
//Palangkaraya
Route::get('/Palangkaraya', [KalimantanController::class, 'Palangkaraya']);
//Samarinda
Route::get('/Samarinda', [KalimantanController::class, 'Samarinda']);
//Balikpapan
Route::get('/Balikpapan', [KalimantanController::class, 'Balikpapan']);
//Banjarmasin
Route::get('/Banjarmasin', [KalimantanController::class, 'Banjarmasin']);

//Bali&Lombok
//Bali
Route::get('/Bali', [BaliLombokController::class, 'Bali']);
//Denpasar
Route::get('/Denpasar', [BaliLombokController::class, 'Denpasar']);
//Lombok
Route::get('/Lombok', [BaliLombokController::class, 'Lombok']);
//Mataram
Route::get('/Mataram', [BaliLombokController::class, 'Mataram']);

//Jawa
//Bandung
Route::get('/Bandung', [JawaController::class, 'Bandung']);
//Banyumas
Route::get('/Banyumas', [JawaController::class, 'Banyumas']);
//Cianjur
Route::get('/Cianjur', [JawaController::class, 'Cianjur']);
//Cirebon
Route::get('/Cirebon', [JawaController::class, 'Cirebon']);
//Garut
Route::get('/Garut', [JawaController::class, 'Garut']);
//Kebumen
Route::get('/Kebumen', [JawaController::class, 'Kebumen']);
//Kediri
Route::get('/Kediri', [JawaController::class, 'Kediri']);
//Kudus
Route::get('/Kudus', [JawaController::class, 'Kudus']);
//Magelang
Route::get('/Magelang', [JawaController::class, 'Magelang']);
//Malang
Route::get('/Malang', [JawaController::class, 'Malang']);
//Pekalongan
Route::get('/Pekalongan', [JawaController::class, 'Pekalongan']);
//Purbalingga
Route::get('/Purbalingga', [JawaController::class, 'Purbalingga']);
//Purwokerto
Route::get('/Purwokerto', [JawaController::class, 'Purwokerto']);
//Rempang
Route::get('/Rempang', [JawaController::class, 'Rempang']);
//Semarang
Route::get('/Semarang', [JawaController::class, 'Semarang']);
//Solo
Route::get('/Solo', [JawaController::class, 'Solo']);
//Sukabumi
Route::get('/Sukabumi', [JawaController::class, 'Sukabumi']);
//Surabaya
Route::get('/Surabaya', [JawaController::class, 'Surabaya']);
//Tasikmalaya
Route::get('/Tasikmalaya', [JawaController::class, 'Tasikmalaya']);
//Tegal
Route::get('/Tegal', [JawaController::class, 'Tegal']);
//Wonogiri
Route::get('/Wonogiri', [JawaController::class, 'Wonogiri']);
//Wonosobo
Route::get('/Wonosobo', [JawaController::class, 'Wonosobo']);
//Yogyakarta
Route::get('/Yogyakarta', [JawaController::class, 'Yogyakarta']);

//Sumatera
//Aceh
Route::get('/Aceh', [SumateraController::class, 'Aceh']);
//Batu Raja
Route::get('/Batu Raja', [SumateraController::class, 'BatuRaja']);
//Bangka
Route::get('/Bangka', [SumateraController::class, 'Bangka']);
//Batam
Route::get('/Batam', [SumateraController::class, 'Batam']);
//Bengkulu
Route::get('/Bengkulu', [SumateraController::class, 'Bengkulu']);
//Bukit Tinggi
Route::get('/Bukit Tinggi', [SumateraController::class, 'BukitTinggi']);
//Jambi
Route::get('/Jambi', [SumateraController::class, 'Jambi']);
//Kotabumi
Route::get('/Kota Bumi', [SumateraController::class, 'Kota Bumi']);
//Lampung
Route::get('/Lampung', [SumateraController::class, 'Lampung']);
//Medan
Route::get('/Medan', [SumateraController::class, 'Medan']);
//Muara Bulian
Route::get('/Muara Bulian', [SumateraController::class, 'MuaraBulian']);
//Padang
Route::get('/Padang', [SumateraController::class, 'Padang']);
//Pekanbaru
Route::get('/Pekanbaru', [SumateraController::class, 'Pekanbaru']);
//Palembang
Route::get('/Palembang', [SumateraController::class, 'Palembang']);
//Pematang Siantar
Route::get('/Pematang Siantar', [SumateraController::class, 'PematangSiantar']);
//Riau
Route::get('/Riau', [SumateraController::class, 'Riau']);
//Tanjung Pinang
Route::get('/TanjungPinang', [SumateraController::class, 'TanjungPinang']);
