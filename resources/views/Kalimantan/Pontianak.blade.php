<!DOCTYPE html>
<html class="no-js" lang="en">
<!-- Mirrored from htmldemo.net/flosun/flosun/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Dec 2022 05:03:14 GMT -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>FLORIST-Pontianak</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('template')}}/images/logoh.png" />

    <!-- CSS
	============================================ -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/bootstrap.min.css" />
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/font.awesome.min.css" />
    <!-- Linear Icons CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/linearicons.min.css" />
    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/swiper-bundle.min.css" />
    <!-- Animation CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/animate.min.css" />
    <!-- Jquery ui CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/jquery-ui.min.css" />
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/nice-select.min.css" />
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/magnific-popup.css" />
    <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" />


    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/style.css" />
</head>



<body style="overflow: visible;" class="">
    <!-- Header Area Start Here -->
    <header class="main-header-area">
        <!-- Main Header Area Start -->
        <div class="main-header header-transparent header-sticky sticky">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-2 col-xl-2 col-md-6 col-6 col-custom">
                        <div class="header-logo d-flex align-items-center">
                            <a href="index.html">
                                <img class="img-full" src="http://127.0.0.1:8000/template/images/logo/logoh.png" alt="Header Logo">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 d-none d-lg-flex justify-content-center col-custom">
                        <nav class="main-nav d-none d-lg-flex">
                            <ul class="nav">
                                <li>
                                    <a class="" href="#">
                                        <span class="menu-text">Sumatera</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <div class="mega-menu dropdown-hover">
                                        <div class="menu-colum">
                                            <ul>
                                                <li><a href="/Aceh">Aceh</a></li>
                                                <li><a href="/Bangka">Bangka</a></li>
                                                <li><a href="/Batam">Batam</a></li>
                                                <li><a href="/Batu Raja">Batu Raja</a></li>
                                                <li><a href="/Bengkulu">Bengkulu</a></li>
                                                <li><a href="/Bukit Tinggi">Bukit Tinggi</a></li>
                                            </ul>
                                        </div>
                                        <div class="menu-colum">
                                            <ul>
                                                <li><a href="/Jambi">Jambi</a></li>
                                                <li><a href="/Kotabumi">Kotabumi</a></li>
                                                <li><a href="/Lampung">Lampung</a></li>
                                                <li><a href="/Medan">Medan</a></li>
                                                <li><a href="/Muara Bulian">Muara Bulian</a></li>
                                            </ul>
                                        </div>
                                        <div class="menu-colum">
                                            <ul>
                                                <li><a href="/Padang">Padang</a></li>
                                                <li><a href="/Pekanbaru">Pekanbaru</a></li>
                                                <li><a href="/Palembang">Palembang</a></li>
                                                <li><a href="/Pematang Siantar">Pematang Siantar</a></li>
                                                <li><a href="/Riau">Riau</a></li>
                                                <li><a href="/Tanjung Pinang">Tanjung Pinang</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                <li>
                                    <a href="#">
                                        <span class="menu-text">Jawa</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <div class="mega-menu dropdown-hover">
                                        <div class="menu-colum">
                                            <ul>
                                                <li><a href="/Bandung">Bandung</a></li>
                                                <li><a href="/Banyumas">Banyumas</a></li>
                                                <li><a href="/Cianjur">Cianjur</a></li>
                                                <li><a href="/Cirebon">Cirebon</a></li>
                                                <li><a href="/Garut">Garut</a></li>
                                                <li><a href="/Kebumen">Kebumen</a></li>
                                                <li><a href="/Kediri">Kediri</a></li>
                                                <li><a href="/Kudus">Kudus</a></li>
                                            </ul>
                                        </div>
                                        <div class="menu-colum">
                                            <ul>
                                                <li><a href="/Magelang">Magelang</a></li>
                                                <li><a href="/Malang">Malang</a></li>
                                                <li><a href="/Pekalongan">Pekalongan</a></li>
                                                <li><a href="/Purbalingga">Purbalingga</a></li>
                                                <li><a href="/Purwokerto">Purwokerto</a></li>
                                                <li><a href="/Rempang">Rempang</a></li>
                                                <li><a href="/Semarang">Semarang</a></li>
                                            </ul>
                                        </div>
                                        <div class="menu-colum">
                                            <ul>
                                                <li><a href="/Solo">Solo</a></li>
                                                <li><a href="/Sukabumi">Sukabumi</a></li>
                                                <li><a href="/Surabaya">Surabaya</a></li>
                                                <li><a href="/Tasikmalaya">Tasikmalaya</a></li>
                                                <li><a href="/Tegal">Tegal</a></li>
                                                <li><a href="/Wonogiri">Wonogiri</a></li>
                                                <li><a href="/Wonosobo">Wonosobo</a></li>
                                                <li><a href="/Yogyakarta">Yogyakarta</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                </li>
                                <li>
                                    <a class="" href="#">
                                        <span class="menu-text"> Kalimantan</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-submenu dropdown-hover">
                                        <li><a href="/Pontianak">Pontianak</a></li>
                                        <li><a href="/Palangkaraya">Palangkaraya</a></li>
                                        <li><a href="/Samarinda">Samarinda</a></li>
                                        <li><a href="/Banjarmasin">Banjarmasin</a></li>
                                        <li><a href="/Balikpapan">Balikpapan</a></li>

                                    </ul>
                                </li>
                                <li>
                                    <a class="" href="#">
                                        <span class="menu-text">Sulawesi</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-submenu dropdown-hover">
                                        <li><a href="/Makasar">Makasar</a></li>
                                        <li><a href="/Palu">Palu</a></li>
                                        <li><a href="Kendari">Kendari</a></li>
                                        <li><a href="/Manado">Manado</a></li>
                                        <li><a href="/Gorontalo">Gorontalo</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="" href="#">
                                        <span class="menu-text"> Bali & Lombok</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-submenu dropdown-hover">
                                        <li><a href="/Bali">Bali</a></li>
                                        <li><a href="/Denpasar">Denpasar</a></li>
                                        <li><a href="/Lombok">Lombok</a></li>
                                        <li><a href="/Mataram">Mataram</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="" href="index.html">
                                        <span class="menu-text"> Home</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-submenu dropdown-hover">
                                        <li><a href="contact-us.html">Contact</a></li>
                                        <li><a href="my-account.html">My Account</a></li>
                                        <li><a href="frequently-questions.html">FAQ</a></li>
                                        <li><a href="login.html">Login</a></li>
                                        <li><a href="register.html">Register</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-2 col-md-6 col-6 col-custom">
                        <div class="header-right-area main-nav">
                            <ul class="nav">
                                <li class="sidemenu-wrap">
                                    <a href="#"><i class="fa fa-search"></i> </a>
                                    <ul class="dropdown-sidemenu dropdown-hover-2 dropdown-search">
                                        <li>
                                            <form action="#">
                                                <input name="search" id="search" placeholder="Search" type="text">
                                                <button type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                <li class="account-menu-wrap d-none d-lg-flex">
                                    <a href="#" class="off-canvas-menu-btn">
                                        <i class="fa fa-bars"></i>
                                    </a>
                                </li>
                                <li class="mobile-menu-btn d-lg-none">
                                    <a class="off-canvas-btn" href="#">
                                        <i class="fa fa-bars"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Header Area End -->
        <!-- off-canvas menu start -->
        <aside class="off-canvas-wrapper" id="mobileMenu">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="btn-close-off-canvas">
                    <i class="fa fa-times"></i>
                </div>
                <div class="off-canvas-inner">
                    <div class="search-box-offcanvas">
                        <form>
                            <input type="text" placeholder="Search product...">
                            <button class="search-btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <!-- mobile menu start -->
                    <div class="mobile-navigation">
                        <!-- mobile menu navigation start -->
                        <nav>
                            <ul class="mobile-menu">
                                <li class="menu-item-has-children">
                                    <a href="#">Sumatera</a>
                                    <ul class="megamenu dropdown" style="display: none;">
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                        <li><a href="/Aceh">Aceh</a></li>
                                        <li><a href="/Bangka">Bangka</a></li>
                                        <li><a href="/Batam">Batam</a></li>
                                        <li><a href="/Batu Raja">Batu Raja</a></li>
                                        <li><a href="/Bengkulu">Bengkulu</a></li>
                                        <li><a href="/Bukit Tinggi">Bukit Tinggi</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Jawa</a>
                                    <ul class="megamenu dropdown" style="display: none;">
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 1</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Bandung">Bandung</a></li>
                                                <li><a href="/Banyumas">Banyumas</a></li>
                                                <li><a href="/Cianjur">Cianjur</a></li>
                                                <li><a href="/Cirebon">Cirebon</a></li>
                                                <li><a href="/Garut">Garut</a></li>
                                                <li><a href="/Kebumen">Kebumen</a></li>
                                                <li><a href="/Kediri">Kediri</a></li>
                                                <li><a href="/Kudus">Kudus</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 2</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Magelang">Magelang</a></li>
                                                <li><a href="/Malang">Malang</a></li>
                                                <li><a href="/Pekalongan">Pekalongan</a></li>
                                                <li><a href="/Purbalingga">Purbalingga</a></li>
                                                <li><a href="/Purwokerto">Purwokerto</a></li>
                                                <li><a href="/Rempang">Rempang</a></li>
                                                <li><a href="/Semarang">Semarang</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 3</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Solo">Solo</a></li>
                                                <li><a href="/Sukabumi">Sukabumi</a></li>
                                                <li><a href="/Surabaya">Surabaya</a></li>
                                                <li><a href="/Tasikmalaya">Tasikmalaya</a></li>
                                                <li><a href="/Tegal">Tegal</a></li>
                                                <li><a href="/Wonogiri">Wonogiri</a></li>
                                                <li><a href="/Wonosobo">Wonosobo</a></li>
                                                <li><a href="/Yogyakarta">Yogyakarta</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Kalimantan</a>
                                    <ul class="dropdown">
                                        <li><a href="/Pontianak">Pontianak</a></li>
                                        <li><a href="/Palangkaraya">Palangkaraya</a></li>
                                        <li><a href="/Samarinda">Samarinda</a></li>
                                        <li><a href="/Banjarmasin">Banjarmasin</a></li>
                                        <li><a href="/Balipapan">Balikpapan</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Sulawesi</a>
                                    <ul class="dropdown" style="display: none;">
                                        <li><a href="/Makasar">Makasar</a></li>
                                        <li><a href="/Palu">Palu</a></li>
                                        <li><a href="Kendari">Kendari</a></li>
                                        <li><a href="/Manado">Manado</a></li>
                                        <li><a href="/Gorontalo">Gorontalo</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Bali&Lombok</a>
                                    <ul class="dropdown" style="display: none;">
                                        <li><a href="/Bali">Bali</a></li>
                                        <li><a href="/Denpasar">Denpasar</a></li>
                                        <li><a href="/Lombok">Lombok</a></li>
                                        <li><a href="/Mataram">Mataram</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <!-- mobile menu navigation end -->
                    </div>
                    <!-- mobile menu end -->
                    <div class="offcanvas-widget-area">
                        <div class="switcher">
                            <div class="language">
                                <span class="switcher-title">Language: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">English</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">German</a></li>
                                                <li><a href="#">French</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="currency">
                                <span class="switcher-title">Currency: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">$ USD</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">€ EUR</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="top-info-wrap text-left text-black">
                            <ul class="address-info">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="info%40yourdomain.html">(1245) 2456 012</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <a href="info%40yourdomain.html">info@yourdomain.com</a>
                                </li>
                            </ul>
                            <div class="widget-social">
                                <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- off-canvas menu end -->
        <!-- off-canvas menu start -->
        <aside class="off-canvas-menu-wrapper" id="sideMenu">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="off-canvas-inner">
                    <div class="btn-close-off-canvas">
                        <i class="fa fa-times"></i>
                    </div>
                    <!-- offcanvas widget area start -->
                    <div class="offcanvas-widget-area">
                        <ul class="menu-top-menu">
                            <li><a href="about-us.html">About Us</a></li>
                        </ul>
                        <p class="desc-content">
                            Selamat datang di Smart Florist, tempat yang membawa Anda ke surga bunga dengan agen tersebar di seluruh Indonesia. Kami memulai perjalanan luar biasa kami sebagai "Pengirim Pesan" cinta dan emosi, menghadirkan keindahan bunga kepada pelanggan tercinta kami.
                            Smart Florist menghadirkan koleksi istimewa berupa buket indah, papan bunga yang elegan, dan berbagai opsi hadiah lainnya. </p>
                        </p>
                        <div class="top-info-wrap text-left text-black">
                            <ul class="address-info">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="info%40yourdomain.html">(1245) 2456 012</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <a href="info%40yourdomain.html">info@yourdomain.com</a>
                                </li>
                            </ul>
                            <div class="widget-social">
                                <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- offcanvas widget area end -->
                </div>
            </div>
        </aside>
        <!-- off-canvas menu end -->

        <!-- Slider/Intro Section End -->
        <!--Categories Area Start-->
        <div class="categories-area pt-40">

            <!--Categories Area End-->
            <!--Product Area Start-->
            <div class="product-area mt-text-2">
                <div class="container custom-area-2 overflow-hidden">
                    <div class="row">
                        <!--Section Title Start-->
                        <div class="col-12 col-custom">
                            <div class="section-title text-center mb-30">
                                <span class="section-title-1">Featured Products</span>
                                <h3 class="section-title-3">PONTIANAK</h3>
                            </div>
                        </div>
                        <!--Section Title End-->
                    </div>
                    <div class="row product-row">
                        <div class="col-12 col-custom">
                            <div class="product-slider swiper-container anime-element-multi swiper-container-initialized swiper-container-horizontal">
                                <div class="swiper-wrapper" id="swiper-wrapper-107a9c410f51ec1821" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                                    <div class="single-item swiper-slide swiper-slide-active" role="group" aria-label="1 / 6" style="width: 315.333px; margin-right: 10px;">
                                        <!--Single Product Start-->
                                        <div class="single-product position-relative mb-30">
                                            <div class="product-image">
                                                <a class="d-block" href="product-details.html">
                                                    <img src="http://127.0.0.1:8000/template/images/product/kalimantan3.png" alt="" class="product-image-1 w-100">
                                                </a>
                                                <span class="onsale">Sale!</span>
                                                <div class="add-action d-flex flex-column position-absolute">
                                                    <a href="compare.html" title="Compare">
                                                        <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                                    </a>
                                                    <a href="wishlist.html" title="Add To Wishlist">
                                                        <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                                    </a>
                                                    <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                        <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2">
                                                        <a href="product-details.html">Flowers daisy pink stick</a>
                                                    </h4>
                                                </div>
                                                <p>
                                                    tes text
                                                </p>
                                                <div class="price-box">
                                                    <span class="regular-price">Rp.120.000</span>
                                                    <span class="old-price"><del>Rp.150.000</del></span>
                                                </div>
                                                <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                            </div>
                                        </div>
                                        <!--Single Product End-->
                                        <!--Single Product Start-->
                                        <div class="single-product position-relative mb-30">
                                            <div class="product-image">
                                                <a class="d-block" href="product-details.html">
                                                    <img src="http://127.0.0.1:8000/template/images/product/kalimantan11.png" alt="" class="product-image-1 w-100">
                                                </a>
                                                <div class="add-action d-flex flex-column position-absolute">
                                                    <a href="compare.html" title="Compare">
                                                        <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                                    </a>
                                                    <a href="wishlist.html" title="Add To Wishlist">
                                                        <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                                    </a>
                                                    <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                        <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2">
                                                        <a href="product-details.html">Jasmine flowers white</a>
                                                    </h4>
                                                </div>
                                                <p>
                                                    tes text
                                                </p>
                                                <div class="price-box">
                                                    <span class="regular-price">Rp.120.000</span>
                                                    <span class="old-price"><del>Rp.150.000</del></span>
                                                </div>
                                                <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                            </div>
                                        </div>
                                        <!--Single Product End-->
                                    </div>
                                    <div class="single-item swiper-slide swiper-slide-next" role="group" aria-label="2 / 6" style="width: 315.333px; margin-right: 10px;">
                                        <!--Single Product Start-->
                                        <div class="single-product position-relative mb-30">
                                            <div class="product-image">
                                                <a class="d-block" href="product-details.html">
                                                    <img src="http://127.0.0.1:8000/template/images/product/kalimantan2.png" alt="" class="product-image-1 w-100">
                                                </a>
                                                <div class="add-action d-flex flex-column position-absolute">
                                                    <a href="compare.html" title="Compare">
                                                        <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                                    </a>
                                                    <a href="wishlist.html" title="Add To Wishlist">
                                                        <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                                    </a>
                                                    <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                        <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2">
                                                        <a href="product-details.html">Blossom bouquet flower</a>
                                                    </h4>
                                                </div>
                                                <p>
                                                    tes text
                                                </p>
                                                <div class="price-box">
                                                    <span class="regular-price">Rp.120.000</span>
                                                    <span class="old-price"><del>Rp.150.000</del></span>
                                                </div>
                                                <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                            </div>
                                        </div>
                                        <!--Single Product End-->
                                        <!--Single Product Start-->
                                        <div class="single-product position-relative mb-30">
                                            <div class="product-image">
                                                <a class="d-block" href="product-details.html">
                                                    <img src="http://127.0.0.1:8000/template/images/product/kalimantan10.png" alt="" class="product-image-1 w-100">
                                                </a>
                                                <div class="add-action d-flex flex-column position-absolute">
                                                    <a href="compare.html" title="Compare">
                                                        <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                                    </a>
                                                    <a href="wishlist.html" title="Add To Wishlist">
                                                        <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                                    </a>
                                                    <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                        <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2">
                                                        <a href="product-details.html">Orchid flower red stick</a>
                                                    </h4>
                                                </div>
                                                <p>
                                                    tes text
                                                </p>
                                                <div class="price-box">
                                                    <span class="regular-price">Rp.80.000</span>
                                                    <span class="old-price"><del>Rp.190.000</del></span>
                                                </div>
                                                <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                            </div>
                                        </div>
                                        <!--Single Product End-->
                                    </div>
                                    <div class="single-item swiper-slide" role="group" aria-label="3 / 6" style="width: 315.333px; margin-right: 10px;">
                                        <!--Single Product Start-->
                                        <div class="single-product position-relative mb-30">
                                            <div class="product-image">
                                                <a class="d-block" href="product-details.html">
                                                    <img src="http://127.0.0.1:8000/template/images/product/kalimantan5.png" alt="" class="product-image-1 w-100">
                                                </a>
                                                <div class="add-action d-flex flex-column position-absolute">
                                                    <a href="compare.html" title="Compare">
                                                        <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                                    </a>
                                                    <a href="wishlist.html" title="Add To Wishlist">
                                                        <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                                    </a>
                                                    <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                        <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2">
                                                        <a href="product-details.html">Rose bouquet white</a>
                                                    </h4>
                                                </div>
                                                <p>
                                                    tes text
                                                </p>
                                                <div class="price-box">
                                                    <span class="regular-price">Rp.120.000</span>
                                                    <span class="old-price"><del>Rp.190.000</del></span>
                                                </div>
                                                <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                            </div>
                                        </div>
                                        <!--Single Product End-->
                                        <!--Single Product Start-->
                                        <div class="single-product position-relative mb-30">
                                            <div class="product-image">
                                                <a class="d-block" href="product-details.html">
                                                    <img src="http://127.0.0.1:8000/template/images/product/kalimantan9.png" alt="" class="product-image-1 w-100">
                                                </a>
                                                <div class="add-action d-flex flex-column position-absolute">
                                                    <a href="compare.html" title="Compare">
                                                        <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                                    </a>
                                                    <a href="wishlist.html" title="Add To Wishlist">
                                                        <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                                    </a>
                                                    <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                        <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2">
                                                        <a href="product-details.html">Hyacinth white stick</a>
                                                    </h4>
                                                </div>
                                                <p>
                                                    tes text
                                                </p>
                                                <div class="price-box">
                                                    <span class="regular-price">Rp.120.000</span>
                                                    <span class="old-price"><del>Rp.190.000</del></span>
                                                </div>
                                                <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                            </div>
                                        </div>
                                        <!--Single Product End-->
                                    </div>
                                    <div class="single-item swiper-slide" role="group" aria-label="4 / 6" style="width: 315.333px; margin-right: 10px;">
                                        <!--Single Product Start-->
                                        <div class="single-product position-relative mb-30">
                                            <div class="product-image">
                                                <a class="d-block" href="product-details.html">
                                                    <img src="http://127.0.0.1:8000/template/images/product/15.png" alt="" class="product-image-1 w-100">
                                                </a>
                                                <div class="add-action d-flex flex-column position-absolute">
                                                    <a href="compare.html" title="Compare">
                                                        <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                                    </a>
                                                    <a href="wishlist.html" title="Add To Wishlist">
                                                        <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                                    </a>
                                                    <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                        <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2">
                                                        <a href="product-details.html">Glory of the Snow</a>
                                                    </h4>
                                                </div>
                                                <p>
                                                    tes text
                                                </p>
                                                <div class="price-box">
                                                    <span class="regular-price">Rp.120.000</span>
                                                    <span class="old-price"><del>Rp.150.000</del></span>
                                                </div>
                                                <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                            </div>
                                        </div>
                                        <!--Single Product End-->
                                        <!--Single Product Start-->
                                        <div class="single-product position-relative mb-30">
                                            <div class="product-image">
                                                <a class="d-block" href="product-details.html">
                                                    <img src="http://127.0.0.1:8000/template/images/product/kalimantan8.png" alt="" class="product-image-1 w-100">
                                                </a>
                                                <div class="add-action d-flex flex-column position-absolute">
                                                    <a href="compare.html" title="Compare">
                                                        <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                                    </a>
                                                    <a href="wishlist.html" title="Add To Wishlist">
                                                        <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                                    </a>
                                                    <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                        <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2">
                                                        <a href="product-details.html">Jack in the Pulpit</a>
                                                    </h4>
                                                </div>
                                                <p>
                                                    tes text
                                                </p>
                                                <div class="price-box">
                                                    <span class="regular-price">Rp.180.000</span>
                                                    <span class="old-price"><del>Rp.190.000</del></span>
                                                </div>
                                                <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                            </div>
                                        </div>
                                        <!--Single Product End-->
                                    </div>
                                    <div class="single-item swiper-slide" role="group" aria-label="5 / 6" style="width: 315.333px; margin-right: 10px;">
                                        <!--Single Product Start-->
                                        <div class="single-product position-relative mb-30">
                                            <div class="product-image">
                                                <a class="d-block" href="product-details.html">
                                                    <img src="http://127.0.0.1:8000/template/images/product/1.png" alt="" class="product-image-1 w-100">
                                                </a>
                                                <div class="add-action d-flex flex-column position-absolute">
                                                    <a href="compare.html" title="Compare">
                                                        <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                                    </a>
                                                    <a href="wishlist.html" title="Add To Wishlist">
                                                        <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                                    </a>
                                                    <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                        <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2">
                                                        <a href="product-details.html">Pearly Everlasting</a>
                                                    </h4>
                                                </div>
                                                <p>
                                                    tes text
                                                </p>
                                                <div class="price-box">
                                                    <span class="regular-price">Rp.180.000</span>
                                                    <span class="old-price"><del>Rp.190.000</del></span>
                                                </div>
                                                <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                            </div>
                                        </div>
                                        <!--Single Product End-->
                                        <!--Single Product Start-->
                                        <div class="single-product position-relative mb-30">
                                            <div class="product-image">
                                                <a class="d-block" href="product-details.html">
                                                    <img src="http://127.0.0.1:8000/template/images/product/kalimantan7.png" alt="" class="product-image-1 w-100">
                                                </a>
                                                <div class="add-action d-flex flex-column position-absolute">
                                                    <a href="compare.html" title="Compare">
                                                        <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                                    </a>
                                                    <a href="wishlist.html" title="Add To Wishlist">
                                                        <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                                    </a>
                                                    <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                        <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2">
                                                        <a href="product-details.html">Flowers daisy pink stick</a>
                                                    </h4>
                                                </div>
                                                <p>
                                                    tes text
                                                </p>
                                                <div class="price-box">
                                                    <span class="regular-price">Rp.180.000</span>
                                                    <span class="old-price"><del>Rp.190.000</del></span>
                                                </div>
                                                <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                            </div>
                                        </div>
                                        <!--Single Product End-->
                                    </div>
                                    <div class="single-item swiper-slide" role="group" aria-label="6 / 6" style="width: 315.333px; margin-right: 10px;">
                                        <!--Single Product Start-->
                                        <div class="single-product position-relative mb-30">
                                            <div class="product-image">
                                                <a class="d-block" href="product-details.html">
                                                    <img src="http://127.0.0.1:8000/template/images/product/2.png" alt="" class="product-image-1 w-100">
                                                </a>
                                                <div class="add-action d-flex flex-column position-absolute">
                                                    <a href="compare.html" title="Compare">
                                                        <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                                    </a>
                                                    <a href="wishlist.html" title="Add To Wishlist">
                                                        <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                                    </a>
                                                    <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                        <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2">
                                                        <a href="product-details.html">Flowers white</a>
                                                    </h4>
                                                </div>
                                                <p>
                                                    tes text
                                                </p>
                                                <div class="price-box">
                                                    <span class="regular-price">Rp.120.000</span>
                                                    <span class="old-price"><del>Rp.190.000</del></span>
                                                </div>
                                                <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                            </div>
                                        </div>
                                        <!--Single Product End-->
                                        <!--Single Product Start-->
                                        <div class="single-product position-relative mb-30">
                                            <div class="product-image">
                                                <a class="d-block" href="product-details.html">
                                                    <img src="http://127.0.0.1:8000/template/images/product/3.png" alt="" class="product-image-1 w-100">
                                                </a>
                                                <div class="add-action d-flex flex-column position-absolute">
                                                    <a href="compare.html" title="Compare">
                                                        <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                                    </a>
                                                    <a href="wishlist.html" title="Add To Wishlist">
                                                        <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                                    </a>
                                                    <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                        <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-title">
                                                    <h4 class="title-2">
                                                        <a href="product-details.html">Flower red stick</a>
                                                    </h4>
                                                </div>
                                                <p>
                                                    tes text
                                                </p>
                                                <div class="price-box">
                                                    <span class="regular-price">Rp.180.000</span>
                                                    <span class="old-price"><del>Rp.190.000</del></span>
                                                </div>
                                                <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                            </div>
                                        </div>
                                        <!--Single Product End-->
                                    </div>
                                </div>
                                <!-- Slider pagination -->
                                <div class="swiper-pagination default-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 4"></span></div>
                                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Product Countdown Area End Here -->
        <!-- History Area Start Here -->
        <div class="our-history-area pt-text-3">
            <div class="container">
                <div class="row">
                    <!--Section Title Start-->

                    <!--Section Title End-->
                </div>
                <div class="row">
                    <div class="col-lg-8 ms-auto me-auto">
                        <div class="history-area-content pb-0 mb-0 border-0 text-center">
                            <p>
                                <strong>PONTIANAK DESKRIPTION</strong>
                            </p>
                            <p>
                                Pusat Bunga Pontianak adalah tujuan utama Anda untuk rangkaian bunga yang eksklusif dan kreasinya yang indah di pulau yang mempesona, Pontianak. Terletak di pusat Pontianak, pusat bunga kami adalah tempat perlindungan bagi para penggemar bunga, menyediakan pilihan buket yang dirancang dengan cermat dan penuh warna untuk setiap kesempatan. Para ahli tukang bunga kami, dengan semangat mendalam terhadap seni mereka, bekerja tanpa lelah untuk membawa keindahan flora yang beragam di Pontianak ke setiap rangkaian. Baik Anda merayakan momen istimewa, menyatakan cinta, atau sekadar membawa kelegaan alam yang elegan ke ruang Anda, Flower Center Pontianak berkomitmen untuk menyajikan rangkaian bunga yang paling segar dan memikat. Masuki tempat perlindungan bunga kami dan biarkan warna dan harumnya membawa Anda ke dunia kebahagiaan botani. Kami mengundang Anda untuk merasakan seni dan kehangatan yang mendefinisikan Flower Center Pontianak - di mana setiap bunga bercerita tentang cinta, keindahan, dan surga tropis yang kami sebut rumah.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- History Area End Here -->
        <!-- Banner Area Start Here -->
        <div class="banner-area mt-text-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-custom">
                        <!--Single Banner Area Start-->
                        <div class="single-banner hover-style mb-30">
                            <div class="banner-img">
                                <a href="#">
                                    <img src="http://127.0.0.1:8000/template/images/banner/1-1.jpg" alt="">
                                    <div class="overlay-1"></div>
                                </a>
                            </div>
                        </div>
                        <!--Single Banner Area End-->
                    </div>
                    <div class="col-md-4 col-custom">
                        <!--Single Banner Area Start-->
                        <div class="single-banner hover-style mb-30">
                            <div class="banner-img">
                                <a href="#">
                                    <img src="http://127.0.0.1:8000/template/images/banner/1-2.jpg" alt="">
                                    <div class="overlay-1"></div>
                                </a>
                            </div>
                        </div>
                        <!--Single Banner Area End-->
                    </div>
                    <div class="col-md-4 col-custom">
                        <!--Single Banner Area Start-->
                        <div class="single-banner hover-style mb-30">
                            <div class="banner-img">
                                <a href="#">
                                    <img src="http://127.0.0.1:8000/template/images/banner/1-3.jpg" alt="">
                                    <div class="overlay-1"></div>
                                </a>
                            </div>
                        </div>
                        <!--Single Banner Area End-->
                    </div>
                </div>
            </div>
        </div>
        <!-- Brand Logo Area End Here -->
        <!--Footer Area Start-->
        <footer class="footer-area">
            <div class="footer-widget-area">
                <div class="container container-default custom-area">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-2 col-custom">
                            <div class="single-footer-widget">
                                <h2 class="widget-title">Information</h2>
                                <ul class="widget-list">
                                    <li><a href="about-us.html">Our Company</a></li>
                                    <li><a href="contact-us.html">Contact Us</a></li>
                                    <li><a href="about-us.html">Our Services</a></li>
                                    <li><a href="about-us.html">Why We?</a></li>
                                    <li><a href="about-us.html">Careers</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-2 col-custom">
                            <div class="single-footer-widget">
                                <h2 class="widget-title">Contact Order</h2>
                                <ul class="widget-list">
                                    <p>
                                        Smart-Florist.com<br>
                                        Toko Bunga Gorontalo<br>
                                        Jl.Garuda Cyber Indonesia<br>
                                        Hotline. 081807077070 (Whatsapp)<br>
                                        Email. sales@Smart-Florist.com</p>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-custom">
                            <div class="single-footer-widget">
                                <h2 class="widget-title">See Information</h2>
                                <div class="widget-body">
                                    <address>
                                        123, ABC, Road ##, Main City, Your address goes here.<br>Phone:
                                        01234 567 890<br>Email: https://example.com
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-copyright-area">
                <div class="container custom-area">
                    <div class="row">
                        <div class="col-12 text-center col-custom">
                            <div class="copyright-content">
                                <p>
                                    kelompok florist
                                    <a href="https://hasthemes.com/" title="https://hasthemes.com/">HasThemes</a>
                                    | Built with&nbsp;<strong>Florist</strong>&nbsp;by
                                    <a href="https://hasthemes.com/" title="https://hasthemes.com/">HasThemes</a>.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--Footer Area End-->

        <!-- Modal -->
        <div class="modal flosun-modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <button type="button" class="close close-button" data-bs-dismiss="modal" aria-label="Close">
                        <span class="close-icon" aria-hidden="true">x</span>
                    </button>
                    <div class="modal-body">
                        <div class="container-fluid custom-area">
                            <div class="row">
                                <div class="col-md-6 col-custom">
                                    <div class="modal-product-img">
                                        <a class="w-100" href="#">
                                            <img class="w-100" src="http://127.0.0.1:8000/template/images/product/large-size/1.jpg" alt="Product">
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scroll to Top Start -->
        <a class="scroll-to-top show" href="#">
            <i class="lnr lnr-arrow-up"></i>
        </a>
        <!-- Scroll to Top End -->

        <!-- JS
    ============================================ -->

        <!-- jQuery JS -->
        <script src="http://127.0.0.1:8000/template/js/vendor/jquery-3.6.0.min.js"></script>
        <!-- jQuery Migrate JS -->
        <script src="http://127.0.0.1:8000/template/js/vendor/jquery-migrate-3.3.2.min.js"></script>
        <!-- Modernizer JS -->
        <script src="http://127.0.0.1:8000/template/js/vendor/modernizr-3.7.1.min.js"></script>
        <!-- Bootstrap JS -->
        <script src="http://127.0.0.1:8000/template/js/vendor/bootstrap.bundle.min.js"></script>

        <!-- Swiper Slider JS -->
        <script src="http://127.0.0.1:8000/template/js/plugins/swiper-bundle.min.js"></script>
        <!-- nice select JS -->
        <script src="http://127.0.0.1:8000/template/js/plugins/nice-select.min.js"></script>
        <!-- Ajaxchimpt js -->
        <script src="http://127.0.0.1:8000/template/js/plugins/jquery.ajaxchimp.min.js"></script>
        <!-- Jquery Ui js -->
        <script src="http://127.0.0.1:8000/template/js/plugins/jquery-ui.min.js"></script>
        <!-- Jquery Countdown js -->
        <script src="http://127.0.0.1:8000/template/js/plugins/jquery.countdown.min.js"></script>
        <!-- jquery magnific popup js -->
        <script src="http://127.0.0.1:8000/template/js/plugins/jquery.magnific-popup.min.js"></script>

        <!-- Main JS -->
        <script src="http://127.0.0.1:8000/template/js/main.js"></script>



</body>

<!-- Mirrored from htmldemo.net/flosun/flosun/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Dec 2022 05:03:14 GMT -->

</html>