@extends('layout/template/navbar')

@section('layout/content')
<!-- Slider/Intro Section End -->
<!--Categories Area Start-->
<div class="categories-area pt-40">

    <!--Categories Area End-->
    <!--Product Area Start-->
    <div class="product-area mt-text-2">
        <div class="container custom-area-2 overflow-hidden">
            <div class="row">
                <!--Section Title Start-->
                <div class="col-12 col-custom">
                    <div class="section-title text-center mb-30">
                        <span class="section-title-1">Featured Products</span>
                        <h3 class="section-title-3">Aceh</h3>
                    </div>
                </div>
                <!--Section Title End-->
            </div>
            <div class="row product-row">
                <div class="col-12 col-custom">
                    <div class="product-slider swiper-container anime-element-multi swiper-container-initialized swiper-container-horizontal" style="overflow: hidden;">
                        <div class="swiper-wrapper" id="swiper-wrapper-107a9c410f51ec1821" aria-live="polite">

                            @foreach($produk as $no => $data)
                            @if($data->wilayah_id == $kode)
                            <div class="single-item swiper-slide" style="width: 315.333px; margin-right: 10px;">
                                <!-- Single Product Start -->
                                <div class="single-product position-relative mb-30">
                                    <div class="product-image">
                                        <a class="d-block" href="product-details.html">
                                            <img src="{{ asset('storage/' . $data->gambar) }}" alt="" class="product-image-1 w-100">
                                        </a>
                                        <span class="onsale">Sale!</span>
                                        <div class="add-action d-flex flex-column position-absolute"></div>
                                    </div>
                                    <div class="product-content">
                                        <div class="product-title">
                                            <h4 class="title-2">
                                                <a href="product-details.html">{{ $data->nama }}</a>
                                            </h4>
                                            <span>{{ $data->deskripsi }}</span>
                                        </div>
                                        <div class="price-box">
                                            <span class="regular-price">Rp.{{ number_format($data->harga, 0, ',', '.') }}</span>
                                        </div>
                                        <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach

                        </div>

                        <!-- Slider pagination -->
                        <div class="swiper-pagination default-pagination swiper-pagination-clickable swiper-pagination-bullets">
                            <!-- ... (pagination bullets) ... -->
                        </div>
                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>
<!-- Product Countdown Area End Here -->
<!-- History Area Start Here -->
<div class="our-history-area pt-text-3">
    <div class="container">
        <div class="row">
            <!--Section Title Start-->

            <!--Section Title End-->
        </div>
        <div class="row">
            <div class="col-lg-8 ms-auto me-auto">
                <div class="history-area-content pb-0 mb-0 border-0 text-center text-start">
                    <h1>
                        ACEH FLOWER CENTER
                    </h1>
                    <p>
                        <strong>Bunga di Aceh. Florist Aceh. Toko Karangan Bunga Aceh.</strong>
                    </p>
                    <p>

                        Pusat Bunga Aceh adalah tujuan utama Anda untuk rangkaian bunga yang eksklusif dan kreasinya yang indah di pulau yang mempesona, Aceh. Terletak di pusat Aceh, pusat bunga kami adalah tempat perlindungan bagi para penggemar bunga, menyediakan pilihan buket yang dirancang dengan cermat dan penuh warna untuk setiap kesempatan. Para ahli tukang bunga kami, dengan semangat mendalam terhadap seni mereka, bekerja tanpa lelah untuk membawa keindahan flora yang beragam di Aceh ke setiap rangkaian. Baik Anda merayakan momen istimewa, menyatakan cinta, atau sekadar membawa kelegaan alam yang elegan ke ruang Anda, Flower Center Aceh berkomitmen untuk menyajikan rangkaian bunga yang paling segar dan memikat. Masuki tempat perlindungan bunga kami dan biarkan warna dan harumnya membawa Anda ke dunia kebahagiaan botani. Kami mengundang Anda untuk merasakan seni dan kehangatan yang mendefinisikan Flower Center - di mana setiap bunga bercerita tentang cinta, keindahan, dan surga tropis yang kami sebut rumah.<br>
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</div>
<!-- History Area End Here -->
<!-- Banner Area Start Here -->
<div class="banner-area mt-text-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-custom">
                <!--Single Banner Area Start-->
                <div class="single-banner hover-style mb-30">
                    <div class="banner-img">
                        <a href="#">
                            <img src="http://127.0.0.1:8000/template/images/banner/1-1.jpg" alt="">
                            <div class="overlay-1"></div>
                        </a>
                    </div>
                </div>
                <!--Single Banner Area End-->
            </div>
            <div class="col-md-4 col-custom">
                <!--Single Banner Area Start-->
                <div class="single-banner hover-style mb-30">
                    <div class="banner-img">
                        <a href="#">
                            <img src="http://127.0.0.1:8000/template/images/banner/1-2.jpg" alt="">
                            <div class="overlay-1"></div>
                        </a>
                    </div>
                </div>
                <!--Single Banner Area End-->
            </div>
            <div class="col-md-4 col-custom">
                <!--Single Banner Area Start-->
                <div class="single-banner hover-style mb-30">
                    <div class="banner-img">
                        <a href="#">
                            <img src="http://127.0.0.1:8000/template/images/banner/1-3.jpg" alt="">
                            <div class="overlay-1"></div>
                        </a>
                    </div>
                </div>
                <!--Single Banner Area End-->
            </div>
        </div>
    </div>
</div>
@endsection