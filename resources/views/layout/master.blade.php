@extends('layout/template/navbar')

@section('layout/content')

<!-- Header Area End Here -->
<!-- Slider/Intro Section Start -->
<div class="intro11-slider-wrap section">
    <div class="intro11-slider swiper-container swiper-container-fade swiper-container-initialized swiper-container-horizontal">
        <div class="swiper-wrapper" id="swiper-wrapper-4dffe3edb3f85147" aria-live="polite" style="transition: all 0ms ease 0s;">
            <div class="intro11-section swiper-slide slide-2 slide-bg-1 bg-position swiper-slide-duplicate swiper-slide-prev swiper-slide-duplicate-next" data-swiper-slide-index="1" role="group" aria-label="1 / 4" style="width: 1059px; transition: all 0ms ease 0s; opacity: 1; transform: translate3d(0px, 0px, 0px);">
                <!-- Intro Content Start -->
                <div class="intro11-content text-left">
                    <h3 class="title-slider black-slider-title text-uppercase">
                        Collection
                    </h3>
                    <h2 class="title">
                        Florist and <br>
                        Birthday Gift
                    </h2>
                    <p class="desc-content">
                        Dengan terus berinovasi dan mengeksplorasi tren terbaru dalam dunia floristika, Smart Florist bukan hanya tempat untuk membeli bunga, tetapi juga destinasi yang mengajak pelanggan untuk merayakan dan membagikan keindahan dalam setiap nuansa hidup.
                    </p>
                    <a href="product-details.html" class="btn flosun-button secondary-btn rounded-0">Shop Now</a>
                </div>
                <!-- Intro Content End -->
            </div>
            <div class="intro11-section swiper-slide slide-6 slide-bg-1 bg-position swiper-slide-active" data-swiper-slide-index="0" role="group" aria-label="2 / 4" style="width: 1059px; transition: all 0ms ease 0s; opacity: 1; transform: translate3d(-1059px, 0px, 0px);">
                <!-- Intro Content Start -->
                <div class="intro11-content text-left">
                    <h3 class="title-slider text-uppercase" style="color: white;">Top Trend</h3>
                    <h2 class="title" style="color: white;">
                        2023 Flowrist Trend
                    </h2>
                    <h4 class="desc-content" style="color: white;">
                        Dengan terus berinovasi dan mengeksplorasi tren terbaru dalam dunia floristika, Smart Florist bukan hanya tempat untuk membeli bunga, tetapi juga destinasi yang mengajak pelanggan untuk merayakan dan membagikan keindahan dalam setiap nuansa hidup.
                    </h4>
                    <a href="product-details.html" class="btn flosun-button secondary-btn theme-color rounded-0">Shop Now</a>
                </div>
                <!-- Intro Content End -->
            </div>
            <div class="intro11-section swiper-slide slide-3 slide-bg-1 bg-position swiper-slide-next swiper-slide-duplicate-prev" data-swiper-slide-index="1" role="group" aria-label="3 / 4" style="width: 1059px; transition: all 0ms ease 0s; opacity: 0; transform: translate3d(-2118px, 0px, 0px);">
                <!-- Intro Content Start -->
                <div class="intro11-content text-left">
                    <h3 class="title-slider black-slider-title text-uppercase">
                        Collection
                    </h3>
                    <h2 class="title">
                        Flowerist and Candle <br>
                        Birthday Gift
                    </h2>
                    <p class="desc-content">
                        Dengan terus berinovasi dan mengeksplorasi tren terbaru dalam dunia floristika, Smart Florist bukan hanya tempat untuk membeli bunga, tetapi juga destinasi yang mengajak pelanggan untuk merayakan dan membagikan keindahan dalam setiap nuansa hidup.
                    </p>
                    <a href="product-details.html" class="btn flosun-button secondary-btn rounded-0">Shop Now</a>
                </div>
                <!-- Intro Content End -->
            </div>
            <div class="intro11-section swiper-slide slide-1 slide-bg-1 bg-position swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="0" role="group" aria-label="4 / 4" style="width: 1059px; transition: all 0ms ease 0s; opacity: 0; transform: translate3d(-3177px, 0px, 0px);">
                <!-- Intro Content Start -->
                <div class="intro11-content text-left">
                    <h3 class="title-slider text-uppercase">Top Trend</h3>
                    <h2 class="title">2023 Florist Trend</h2>
                    <p class="desc-content">
                        Dengan terus berinovasi dan mengeksplorasi tren terbaru dalam dunia floristika, Smart Florist bukan hanya tempat untuk membeli bunga, tetapi juga destinasi yang mengajak pelanggan untuk merayakan dan membagikan keindahan dalam setiap nuansa hidup.
                    </p>
                    <a href="product-details.html" class="btn flosun-button secondary-btn theme-color rounded-0">Shop Now</a>
                </div>
                <!-- Intro Content End -->
            </div>
        </div>
        <!-- Slider Navigation -->
        <div class="home1-slider-prev swiper-button-prev main-slider-nav" tabindex="0" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-4dffe3edb3f85147">
            <i class="lnr lnr-arrow-left"></i>
        </div>
        <div class="home1-slider-next swiper-button-next main-slider-nav" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-4dffe3edb3f85147">
            <i class="lnr lnr-arrow-right"></i>
        </div>
        <!-- Slider pagination -->
        <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span></div>
        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
    </div>
</div>
<!-- Slider/Intro Section End -->
<!--Categories Area Start-->

<!--Categories Area End-->

<!--Product Area Start1-->
<div class="product-area mt-text-2">
    <div class="container custom-area-2 overflow-hidden">
        <div class="row">
            <!--Section Title Start1-->
            <div class="col-12 col-custom">
                <div class="section-title text-center mb-30">
                    <span class="section-title-1">Wonderful gift</span>
                    <h3 class="section-title-3">Best Seller</h3>
                </div>
            </div>
            <!--Section Title End-->
        </div>
        <div class="row product-row">
            <div class="col-12 col-custom">
                <div class="product-slider swiper-container anime-element-multi swiper-container-initialized swiper-container-horizontal" style="overflow: hidden;">
                    <div class="swiper-wrapper" id="swiper-wrapper-107a9c410f51ec1821" aria-live="polite">

                        @foreach($produk as $no => $data)
                        <div class="single-item swiper-slide" style="width: 315.333px; margin-right: 10px;">
                            <!-- Single Product Start -->
                            <div class="single-product position-relative mb-30">
                                <div class="product-image">
                                    <a class="d-block" href="product-details.html">
                                        <img src="{{ asset('storage/' . $data->gambar) }}" alt="" class="product-image-1 w-100">
                                    </a>
                                    <span class="onsale">Sale!</span>
                                    <div class="add-action d-flex flex-column position-absolute"></div>
                                </div>
                                <div class="product-content">
                                    <div class="product-title">
                                        <h4 class="title-2">
                                            <a href="product-details.html">{{ $data->nama }}</a>
                                        </h4>
                                        <span>{{ $data->deskripsi }}</span>
                                    </div>
                                    <div class="price-box">
                                        <span class="regular-price">Rp.{{ number_format($data->harga, 0, ',', '.') }}</span>
                                    </div>
                                    <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>

                    <!-- Slider pagination -->
                    <div class="swiper-pagination default-pagination swiper-pagination-clickable swiper-pagination-bullets">
                        <!-- ... (pagination bullets) ... -->
                    </div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>


            </div>

        </div>
    </div>
</div>
<!--Product Area End-->

<!-- Product Countdown Area Start Here -->
<div class="product-countdown-area mt-text-3">
    <div class="container custom-area">
        <div class="row">
            <!--Section Title Start-->
            <div class="col-10 col-custom">
                <div class="section-title text-center mb-30">
                    <h3 class="section-title-3">New Collaction</h3>
                </div>
            </div>

            <!--Countdown Start-->
            <div class="col-12 col-custom">
            </div>
            <!--Countdown End-->
        </div>
        <div class="row product-row">
            <div class="col-12 col-custom">
                <div class="item-carousel-2 swiper-container anime-element-multi product-area swiper-container-initialized swiper-container-horizontal swiper-container-autoheight">
                    <div class="swiper-wrapper" id="swiper-wrapper-325e6103e4cee49ce" aria-live="polite" style="height: 584px; transform: translate3d(0px, 0px, 0px);">
                        <div class="single-item swiper-slide swiper-slide-active" role="group" aria-label="1 / 5" style="width: 465px;">
                            <!--Single Product Start-->
                            <div class="single-product position-relative mb-30">
                                <div class="product-image">
                                    <a class="d-block" href="product-details.html">
                                        <img src="http://127.0.0.1:8000/template/images/product/154.png" alt="" class="product-image-1 w-100">
                                    </a>
                                    <span class="onsale">Sale!</span>
                                    <div class="add-action d-flex flex-column position-absolute">
                                    </div>
                                </div>
                                <div class="product-content">
                                    <div class="product-title">
                                        <h4 class="title-2">
                                            <a href="product-details.html">Flowers Bucket pink stick</a>
                                        </h4>
                                    </div>
                                    <span>Warnai Hidupmu dengan Koleksi Bunga Terbaru kami.<span>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.180.000</span>
                                                <span class="old-price"><del>Rp.190.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                </div>
                            </div>
                            <!--Single Product End-->
                        </div>
                        <div class="single-item swiper-slide swiper-slide-next" role="group" aria-label="2 / 5" style="width: 465px;">
                            <!--Single Product Start-->
                            <div class="single-product position-relative mb-30">
                                <div class="product-image">
                                    <a class="d-block" href="product-details.html">
                                        <img src="http://127.0.0.1:8000/template/images/product/321 (2).png" alt="" class="product-image-1 w-100">
                                    </a>
                                    <div class="add-action d-flex flex-column position-absolute">
                                    </div>
                                </div>
                                <div class="product-content">
                                    <div class="product-title">
                                        <h4 class="title-2">
                                            <a href="product-details.html">Flowers Board white</a>
                                        </h4>
                                    </div>
                                    <span>Warnai Hidupmu dengan Koleksi Bunga Terbaru kami.<span>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.180.000</span>
                                                <span class="old-price"><del>Rp.190.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                </div>
                            </div>
                            <!--Single Product End-->
                        </div>
                        <div class="single-item swiper-slide" role="group" aria-label="3 / 5" style="width: 465px;">
                            <!--Single Product Start-->
                            <div class="single-product position-relative mb-30">
                                <div class="product-image">
                                    <a class="d-block" href="product-details.html">
                                        <img src="http://127.0.0.1:8000/template/images/product/gghhh-removebg-preview.png" alt="" class="product-image-1 w-100">
                                    </a>
                                    <div class="add-action d-flex flex-column position-absolute">
                                        <a href="compare.html" title="Compare">
                                            <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                        </a>
                                        <a href="wishlist.html" title="Add To Wishlist">
                                            <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                        </a>
                                        <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                            <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <div class="product-title">
                                        <h4 class="title-2">
                                            <a href="product-details.html">Blossom flower bucket</a>
                                        </h4>
                                    </div>
                                    <span>Warnai Hidupmu dengan Koleksi Bunga Terbaru kami.<span>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.180.000</span>
                                                <span class="old-price"><del>Rp.190.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                </div>
                            </div>
                            <!--Single Product End-->
                        </div>
                        <div class="single-item swiper-slide" role="group" aria-label="4 / 5" style="width: 465px;">
                            <!--Single Product Start-->
                            <div class="single-product position-relative mb-30">
                                <div class="product-image">
                                    <a class="d-block" href="product-details.html">
                                        <img src="http://127.0.0.1:8000/template/images/product/kalimantan101.png" alt="" class="product-image-1 w-100">
                                    </a>
                                    <div class="add-action d-flex flex-column position-absolute">
                                        <a href="compare.html" title="Compare">
                                            <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" aria-label="Compare" data-bs-original-title="Compare"></i>
                                        </a>
                                        <a href="wishlist.html" title="Add To Wishlist">
                                            <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" aria-label="Wishlist" data-bs-original-title="Wishlist"></i>
                                        </a>
                                        <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                            <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" aria-label="Quick View" data-bs-original-title="Quick View"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <div class="product-title">
                                        <h4 class="title-2">
                                            <a href="product-details.html">Flower red board</a>
                                        </h4>
                                    </div>
                                    <span>Warnai Hidupmu dengan Koleksi Bunga Terbaru kami.<span>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.180.000</span>
                                                <span class="old-price"><del>Rp.190.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                </div>
                            </div>
                            <!--Single Product End-->
                        </div>
                        <div class="single-item swiper-slide" role="group" aria-label="5 / 5" style="width: 465px;">
                            <!--Single Product Start-->
                            <div class="single-product position-relative mb-30">
                                <div class="product-image">
                                    <a class="d-block" href="product-details.html">
                                        <img src="http://127.0.0.1:8000/template/images/product/321 (1).png" alt="" class="product-image-1 w-100">
                                    </a>
                                    <div class="add-action d-flex flex-column position-absolute">
                                    </div>
                                </div>
                                <div class="product-content">
                                    <div class="product-title">
                                        <h4 class="title-2">
                                            <a href="product-details.html">Rose bouquet white</a>
                                        </h4>
                                    </div>
                                    <span>Warnai Hidupmu dengan Koleksi Bunga Terbaru kami.<span>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.180.000</span>
                                                <span class="old-price"><del>Rp.190.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                </div>
                            </div>
                            <!--Single Product End-->
                        </div>
                    </div>
                    <!-- Slider pagination -->
                    <div class="swiper-pagination default-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 4"></span></div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Product Countdown Area End Here -->
<!-- History Area Start Here -->
<div class="our-history-area pt-text-3">
    <div class="container">
        <div class="row">
            <!--Section Title Start-->
            <div class="col-12">
                <div class="section-title text-center mb-30">
                    <span class="section-title-1">A little Story About Us</span>
                    <h2 class="section-title-large">Our History</h2>
                </div>
            </div>
            <!--Section Title End-->
        </div>
        <div class="row">
            <div class="col-lg-8 ms-auto me-auto">
                <div class="history-area-content pb-0 mb-0 border-0 text-center">
                    <p>
                        <strong>Smart Florist, sebuah platform florist online yang menghadirkan keindahan bunga dengan sentuhan teknologi modern. Di balik antarmuka yang ramah pengguna, Smart Florist tidak hanya menawarkan koleksi bunga yang elegan dan beragam, tetapi juga memberikan pengalaman belanja yang cerdas. Dengan fitur pencarian yang pintar dan pengiriman yang efisien, pelanggan dapat dengan mudah menemukan dan mendapatkan rangkaian bunga yang sesuai dengan setiap momen istimewa. Inovatif dalam pendekatan teknologinya, Smart Florist membawa seni floristika ke era digital, menjadikan setiap pembelian bunga sebuah pengalaman yang indah dan terkoneksi secara modern.</strong>
                    </p>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- History Area End Here -->
<!-- Banner Area Start Here -->
<div class="banner-area mt-text-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-custom">
                <!--Single Banner Area Start-->
                <div class="single-banner hover-style mb-30">
                    <div class="banner-img">
                        <a href="#">
                            <img src="http://127.0.0.1:8000/template/images/banner/1-1.jpg" alt="">
                            <div class="overlay-1"></div>
                        </a>
                    </div>
                </div>
                <!--Single Banner Area End-->
            </div>
            <div class="col-md-4 col-custom">
                <!--Single Banner Area Start-->
                <div class="single-banner hover-style mb-30">
                    <div class="banner-img">
                        <a href="#">
                            <img src="http://127.0.0.1:8000/template/images/banner/1-2.jpg" alt="">
                            <div class="overlay-1"></div>
                        </a>
                    </div>
                </div>
                <!--Single Banner Area End-->
            </div>
            <div class="col-md-4 col-custom">
                <!--Single Banner Area Start-->
                <div class="single-banner hover-style mb-30">
                    <div class="banner-img">
                        <a href="#">
                            <img src="http://127.0.0.1:8000/template/images/banner/1-3.jpg" alt="">
                            <div class="overlay-1"></div>
                        </a>
                    </div>
                </div>
                <!--Single Banner Area End-->
            </div>
        </div>
    </div>
</div>
<!-- Shop Collection Start Here -->
<div class="about-area mt-no-text">
    <div class="container custom-area">
        <div class="row mb-30 align-items-center">
            <div class="col-md-6 col-custom">
                <div class="collection-content">
                    <div class="section-title text-left">
                        <span class="section-title-1">Flowers for the</span>
                        <h3 class="section-title-3 pb-0">Birthday &amp; Gifts</h3>
                    </div>
                    <div class="desc-content">
                        <p>
                            <strong>Bunga yang memesona adalah petikan bisikan bahagia di hari ulang tahunmu. Produk bunga kami menggambarkan sentuhan kecantikan dan kebahagiaan untuk merayakan momen spesial dalam hidupmu.</strong>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-custom">
                <!--Single Banner Area Start-->
                <div class="single-banner hover-style">
                    <div class="banner-img">
                        <a href="#">
                            <img src="http://127.0.0.1:8000/template/images/collection/1-11.jpg" alt="About Image">
                            <div class="overlay-1"></div>
                        </a>
                    </div>
                </div>
                <!--Single Banner Area End-->
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-md-6 col-custom order-2 order-md-1">
                <!--Single Banner Area Start-->
                <div class="single-banner hover-style">
                    <div class="banner-img">
                        <a href="#">
                            <img src="http://127.0.0.1:8000/template/images/collection/1-22.jpg" alt="About Image">
                            <div class="overlay-1"></div>
                        </a>
                    </div>
                </div>
                <!--Single Banner Area End-->
            </div>
            <div class="col-md-6 col-custom order-1 order-md-2">
                <div class="collection-content">
                    <div class="section-title text-left">
                        <span class="section-title-1">Flowers for the</span>
                        <h3 class="section-title-3 pb-0">Wedding day</h3>
                    </div>
                    <div class="desc-content">
                        <p>
                            <strong>Bunga yang indah adalah bahasa diam cinta sejati. Produk bunga
                                kami adalah cara kami menyampaikan keindahan dan kehangatan
                                dalam pernikahan Anda.</strong>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Blog Area End Here -->
@endsection