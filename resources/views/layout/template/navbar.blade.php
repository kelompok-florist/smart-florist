<!DOCTYPE html>
<html class="no-js" lang="en">
<!-- Mirrored from htmldemo.net/flosun/flosun/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Dec 2022 05:03:14 GMT -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>Smart Florist</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('template')}}/images/logo/logow.png" />

    <!-- CSS
	============================================ -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/bootstrap.min.css" />
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/font.awesome.min.css" />
    <!-- Linear Icons CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/vendor/linearicons.min.css" />
    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/swiper-bundle.min.css" />
    <!-- Animation CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/animate.min.css" />
    <!-- Jquery ui CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/jquery-ui.min.css" />
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/nice-select.min.css" />
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('template')}}/css/plugins/magnific-popup.css" />
    <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" />


    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{asset('template')}}/css/style.css" />
</head>



<body style="overflow: visible;" class="">
    <!-- Header Area Start Here -->
    <header class="main-header-area">
        <!-- Main Header Area Start -->
        <div class="main-header header-transparent header-sticky sticky">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-2 col-xl-2 col-md-6 col-6 col-custom">
                        <div class="header-logo d-flex align-items-center">
                            <a href="index.html">
                                <img class="img-full" src="http://127.0.0.1:8000/template/images/logo/logow.png" alt="Header Logo">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-8 d-none d-lg-flex justify-content-center col-custom">
                        <nav class="main-nav d-none d-lg-flex">
                            <ul class="nav">
                                @foreach ($pulau as $no => $data)
                                <li>
                                    <a class="" href="#">
                                        <span class="menu-text">{{ $data->nama_pulau }}</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <div class="mega-menu dropdown-hover">
                                        <div class="menu-colum">
                                            <ul>
                                                <?php foreach ($wilayah as $item) { ?>
                                                    @if($item->pulau == $data->nama_pulau)
                                                    <li><a href="/Aceh">{{ $item->nama }}</a></li>
                                                    @endif
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                                
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-2 col-md-6 col-6 col-custom">
                        <div class="header-right-area main-nav">
                            <ul class="nav">
                                <li class="sidemenu-wrap">
                                    <a href="#"><i class="fa fa-search"></i> </a>
                                    <ul class="dropdown-sidemenu dropdown-hover-2 dropdown-search">
                                        <li>
                                            <form action="#">
                                                <input name="search" id="search" placeholder="Search" type="text">
                                                <button type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                <li class="account-menu-wrap d-none d-lg-flex">
                                    <a href="#" class="off-canvas-menu-btn">
                                        <i class="fa fa-bars"></i>
                                    </a>
                                </li>
                                <li class="mobile-menu-btn d-lg-none">
                                    <a class="off-canvas-btn" href="#">
                                        <i class="fa fa-bars"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Header Area End -->
        <!-- off-canvas menu start -->
        <aside class="off-canvas-wrapper" id="mobileMenu">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="btn-close-off-canvas">
                    <i class="fa fa-times"></i>
                </div>
                <div class="off-canvas-inner">
                    <div class="search-box-offcanvas">
                        <form>
                            <input type="text" placeholder="Search product...">
                            <button class="search-btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <!-- mobile menu start -->
                    <div class="mobile-navigation">
                        <!-- mobile menu navigation start -->
                        <nav>
                            <ul class="mobile-menu">
                                <li class="menu-item-has-children">
                                    <a href="#">Sumatera</a>
                                    <ul class="megamenu dropdown" style="display: none;">
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                        <li><a href="/Aceh">Aceh</a></li>
                                        <li><a href="/Bangka">Bangka</a></li>
                                        <li><a href="/Batam">Batam</a></li>
                                        <li><a href="/Batu Raja">Batu Raja</a></li>
                                        <li><a href="/Bengkulu">Bengkulu</a></li>
                                        <li><a href="/Bukit Tinggi">Bukit Tinggi</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Jawa</a>
                                    <ul class="megamenu dropdown" style="display: none;">
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 1</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Bandung">Bandung</a></li>
                                                <li><a href="/Banyumas">Banyumas</a></li>
                                                <li><a href="/Cianjur">Cianjur</a></li>
                                                <li><a href="/Cirebon">Cirebon</a></li>
                                                <li><a href="/Garut">Garut</a></li>
                                                <li><a href="/Kebumen">Kebumen</a></li>
                                                <li><a href="/Kediri">Kediri</a></li>
                                                <li><a href="/Kudus">Kudus</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 2</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Magelang">Magelang</a></li>
                                                <li><a href="/Malang">Malang</a></li>
                                                <li><a href="/Pekalongan">Pekalongan</a></li>
                                                <li><a href="/Purbalingga">Purbalingga</a></li>
                                                <li><a href="/Purwokerto">Purwokerto</a></li>
                                                <li><a href="/Rempang">Rempang</a></li>
                                                <li><a href="/Semarang">Semarang</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children"><span class="menu-expand"><i></i></span>
                                            <a href="#">Slide 3</a>
                                            <ul class="dropdown" style="display: none;">
                                                <li><a href="/Solo">Solo</a></li>
                                                <li><a href="/Sukabumi">Sukabumi</a></li>
                                                <li><a href="/Surabaya">Surabaya</a></li>
                                                <li><a href="/Tasikmalaya">Tasikmalaya</a></li>
                                                <li><a href="/Tegal">Tegal</a></li>
                                                <li><a href="/Wonogiri">Wonogiri</a></li>
                                                <li><a href="/Wonosobo">Wonosobo</a></li>
                                                <li><a href="/Yogyakarta">Yogyakarta</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Kalimantan</a>
                                    <ul class="dropdown">
                                        <li><a href="/Pontianak">Pontianak</a></li>
                                        <li><a href="/Palangkaraya">Palangkaraya</a></li>
                                        <li><a href="/Samarinda">Samarinda</a></li>
                                        <li><a href="/Banjarmasin">Banjarmasin</a></li>
                                        <li><a href="/Balipapan">Balikpapan</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Sulawesi</a>
                                    <ul class="dropdown" style="display: none;">
                                        <li><a href="/Makasar">Makasar</a></li>
                                        <li><a href="/Palu">Palu</a></li>
                                        <li><a href="Kendari">Kendari</a></li>
                                        <li><a href="/Manado">Manado</a></li>
                                        <li><a href="/Gorontalo">Gorontalo</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><span class="menu-expand"><i></i></span>
                                    <a href="#">Bali&Lombok</a>
                                    <ul class="dropdown" style="display: none;">
                                        <li><a href="/Bali">Bali</a></li>
                                        <li><a href="/Denpasar">Denpasar</a></li>
                                        <li><a href="/Lombok">Lombok</a></li>
                                        <li><a href="/Mataram">Mataram</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <!-- mobile menu navigation end -->
                    </div>
                    <!-- mobile menu end -->
                    <div class="offcanvas-widget-area">

                        <div class="top-info-wrap text-left text-black">
                            <ul class="address-info">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="info%40yourdomain.html">(1245) 2456 012</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <a href="info%40yourdomain.html">smartflorist@gmail.com</a>
                                </li>
                            </ul>
                            <div class="widget-social">
                                <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- off-canvas menu end -->
        <!-- off-canvas menu start -->
        <aside class="off-canvas-menu-wrapper" id="sideMenu">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="off-canvas-inner">
                    <div class="btn-close-off-canvas">
                        <i class="fa fa-times"></i>
                    </div>
                    <!-- offcanvas widget area start -->
                    <div class="offcanvas-widget-area">
                        <ul class="menu-top-menu">
                            <li><a href="about-us.html">About Us</a></li>
                        </ul>
                        <p class="desc-content">
                            Smart Florist, sebuah platform florist online yang menghadirkan keindahan bunga dengan sentuhan teknologi modern. Di balik antarmuka yang ramah pengguna, Smart Florist tidak hanya menawarkan koleksi bunga yang elegan dan beragam, tetapi juga memberikan pengalaman belanja yang cerdas. Dengan fitur pencarian yang pintar dan pengiriman yang efisien, pelanggan dapat dengan mudah menemukan dan mendapatkan rangkaian bunga yang sesuai dengan setiap momen istimewa. Inovatif dalam pendekatan teknologinya, Smart Florist membawa seni floristika ke era digital, menjadikan setiap pembelian bunga sebuah pengalaman yang indah dan terkoneksi secara modern.</strong>
                        </p>
                        <div class="switcher">
                            <div class="language">
                                <span class="switcher-title">Language: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">English</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">Indonesia</a></li>

                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="currency">
                                <span class="switcher-title">Currency: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">$ USD</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">Rp</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="top-info-wrap text-left text-black">
                            <ul class="address-info">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="info%40yourdomain.html">(1245) 2456 012</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <a href="info%40yourdomain.html">info@yourdomain.com</a>
                                </li>
                            </ul>
                            <div class="widget-social">
                                <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- offcanvas widget area end -->
                </div>
            </div>
        </aside>
        <!-- off-canvas menu end -->
        @yield('layout/content')

        @extends('layout/template/footer')