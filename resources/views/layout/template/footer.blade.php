        <!-- Brand Logo Area Start Here -->
        <div class="brand-logo-area gray-bg pt-no-text pb-no-text mt-text-5">
            <div class="container custom-area">
                <div class="row">
                    <div class="col-12 col-custom">
                        <div class="brand-logo-carousel swiper-container intro11-carousel-wrap arrow-style-3 swiper-container-initialized swiper-container-horizontal">
                            <div class="swiper-wrapper" id="swiper-wrapper-7d3610105e1e810ca3e" aria-live="polite" style="transform: translate3d(-940px, 0px, 0px); transition: all 0ms ease 0s;">
                                <div class="single-brand swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next" role="group" aria-label="7 / 15" style="width: 225px; margin-right: 10px;" data-swiper-slide-index="1">
                                    <a href="#">
                                        <img src="http://127.0.0.1:8000/template/images/brand/2.png" alt="Brand Logo">
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide swiper-slide-duplicate" role="group" aria-label="8 / 15" style="width: 225px; margin-right: 10px;" data-swiper-slide-index="2">
                                    <a href="#">
                                        <img src="http://127.0.0.1:8000/template/images/brand/3.png" alt="Brand Logo">
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide swiper-slide-duplicate" role="group" aria-label="9 / 15" style="width: 225px; margin-right: 10px;" data-swiper-slide-index="3">
                                    <a href="#">
                                        <img src="http://127.0.0.1:8000/template/images/brand/4.png" alt="Brand Logo">
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide swiper-slide-duplicate swiper-slide-prev" role="group" aria-label="10 / 15" style="width: 225px; margin-right: 10px;" data-swiper-slide-index="4">
                                    <a href="#">
                                        <img src="http://127.0.0.1:8000/template/images/brand/5.png" alt="Brand Logo">
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide swiper-slide-active" role="group" aria-label="6 / 15" style="width: 225px; margin-right: 10px;" data-swiper-slide-index="0">
                                    <a href="#">
                                        <img src="http://127.0.0.1:8000/template/images/brand/1.png" alt="Brand Logo">
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide swiper-slide-next" role="group" aria-label="7 / 15" style="width: 225px; margin-right: 10px;" data-swiper-slide-index="1">
                                    <a href="#">
                                        <img src="http://127.0.0.1:8000/template/images/brand/2.png" alt="Brand Logo">
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide" role="group" aria-label="8 / 15" style="width: 225px; margin-right: 10px;" data-swiper-slide-index="2">
                                    <a href="#">
                                        <img src="http://127.0.0.1:8000/template/images/brand/3.png" alt="Brand Logo">
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide" role="group" aria-label="9 / 15" style="width: 225px; margin-right: 10px;" data-swiper-slide-index="3">
                                    <a href="#">
                                        <img src="http://127.0.0.1:8000/template/images/brand/4.png" alt="Brand Logo">
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide swiper-slide-duplicate-prev" role="group" aria-label="10 / 15" style="width: 225px; margin-right: 10px;" data-swiper-slide-index="4">
                                    <a href="#">
                                        <img src="http://127.0.0.1:8000/template/images/brand/5.png" alt="Brand Logo">
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" role="group" aria-label="6 / 15" style="width: 225px; margin-right: 10px;" data-swiper-slide-index="0">
                                    <a href="#">
                                        <img src="http://127.0.0.1:8000/template/images/brand/1.png" alt="Brand Logo">
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next" role="group" aria-label="7 / 15" style="width: 225px; margin-right: 10px;" data-swiper-slide-index="1">
                                    <a href="#">
                                        <img src="http://127.0.0.1:8000/template/images/brand/2.png" alt="Brand Logo">
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide swiper-slide-duplicate" role="group" aria-label="8 / 15" style="width: 225px; margin-right: 10px;" data-swiper-slide-index="2">
                                    <a href="#">
                                        <img src="http://127.0.0.1:8000/template/images/brand/3.png" alt="Brand Logo">
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide swiper-slide-duplicate" role="group" aria-label="9 / 15" style="width: 225px; margin-right: 10px;" data-swiper-slide-index="3">
                                    <a href="#">
                                        <img src="http://127.0.0.1:8000/template/images/brand/4.png" alt="Brand Logo">
                                    </a>
                                </div>
                            </div>
                            <!-- Slider Navigation -->
                            <div class="home1-slider-prev swiper-button-prev main-slider-nav" tabindex="0" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-7d3610105e1e810ca3e">
                                <i class="lnr lnr-arrow-left"></i>
                            </div>
                            <div class="home1-slider-next swiper-button-next main-slider-nav" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-7d3610105e1e810ca3e">
                                <i class="lnr lnr-arrow-right"></i>
                            </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Brand Logo Area End Here -->


        <!-- Modal -->
        <div class="modal flosun-modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <button type="button" class="close close-button" data-bs-dismiss="modal" aria-label="Close">
                        <span class="close-icon" aria-hidden="true">x</span>
                    </button>
                    <div class="modal-body">
                        <div class="container-fluid custom-area">
                            <div class="row">
                                <div class="col-md-6 col-custom">
                                    <div class="modal-product-img">
                                        <a class="w-100" href="#">
                                            <img class="w-100" src="http://127.0.0.1:8000/template/images/product/large-size/1.jpg" alt="Product">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-custom">
                                    <div class="modal-product">
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title">Product dummy name</h4>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">$80.00</span>
                                                <span class="old-price"><del>$90.00</del></span>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <span>1 Review</span>
                                            </div>
                                            <p class="desc-content">
                                                we denounce with righteous indignation and dislike men
                                                who are so beguiled and demoralized by the charms of
                                                pleasure of the moment, so blinded by desire, that they
                                                cannot foresee the pain and trouble that are bound to
                                                ensue; and equal blame bel...
                                            </p>
                                            <form class="d-flex flex-column w-100" action="#">
                                                <div class="form-group">
                                                    <select class="form-control nice-select w-100" style="display: none;">
                                                        <option>S</option>
                                                        <option>M</option>
                                                        <option>L</option>
                                                        <option>XL</option>
                                                        <option>XXL</option>
                                                    </select>
                                                    <div class="nice-select form-control w-100" tabindex="0"><span class="current">S</span>
                                                        <ul class="list">
                                                            <li data-value="S" class="option selected">S</li>
                                                            <li data-value="M" class="option">M</li>
                                                            <li data-value="L" class="option">L</li>
                                                            <li data-value="XL" class="option">XL</li>
                                                            <li data-value="XXL" class="option">XXL</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="quantity-with-btn">
                                                <div class="quantity">
                                                    <div class="cart-plus-minus">
                                                        <input class="cart-plus-minus-box" value="0" type="text">
                                                        <div class="dec qtybutton">-</div>
                                                        <div class="inc qtybutton">+</div>
                                                        <div class="dec qtybutton">
                                                            <i class="fa fa-minus"></i>
                                                        </div>
                                                        <div class="inc qtybutton">
                                                            <i class="fa fa-plus"></i>
                                                        </div>
                                                        <div class="dec qtybutton"><i class="fa fa-minus"></i></div>
                                                        <div class="inc qtybutton"><i class="fa fa-plus"></i></div>
                                                    </div>
                                                </div>
                                                <div class="add-to_btn">
                                                    <a class="btn product-cart button-icon flosun-button dark-btn" href="cart.html">Add to cart</a>
                                                    <a class="btn flosun-button secondary-btn rounded-0" href="wishlist.html">Add to wishlist</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scroll to Top Start -->
        <a class="scroll-to-top show" href="#">
            <i class="lnr lnr-arrow-up"></i>
        </a>

        <div class="brand-logo-area gray-bg pt-no-text pb-no-text mt-text-5">

        </div>
        <!-- Brand Logo Area End Here -->
        <!--Footer Area Start-->
        <footer class="footer-area">
            <div class="footer-widget-area">
                <div class="container container-default custom-area">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-2 col-custom">
                            <div class="single-footer-widget">
                                <h2 class="widget-title">Information</h2>
                                <ul class="widget-list">
                                    <li><a href="about-us.html">Our Company</a></li>
                                    <li><a href="contact-us.html">Contact Us</a></li>
                                    <li><a href="about-us.html">Our Services</a></li>
                                    <li><a href="about-us.html">Why We?</a></li>
                                    <li><a href="about-us.html">Careers</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-2 col-custom">
                            <div class="single-footer-widget">
                                <h2 class="widget-title">Contact Order</h2>
                                <ul class="widget-list">
                                    <p>
                                        Smart-Florist.com<br>
                                        Toko Bunga Aceh<br>
                                        Jl.Garuda Cyber Indonesia<br>
                                        Hotline. 081807077070 (Whatsapp)<br>
                                        Email. sales@Smart-Florist.com</p>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-custom">
                            <div class="single-footer-widget">
                                <h2 class="widget-title">See Information</h2>
                                <div class="widget-body">
                                    <address>
                                        123, ABC, Road ##, Main City, Your address goes here.<br>Phone:
                                        01234 567 890<br>Email: https://example.com
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-copyright-area">
                <div class="container custom-area">
                    <div class="row">
                        <div class="col-12 text-center col-custom">
                            <div class="copyright-content">
                                <p>
                                    kelompok florist
                                    <a href="https://hasthemes.com/" title="https://hasthemes.com/">HasThemes</a>
                                    | Built with&nbsp;<strong>Florist</strong>&nbsp;by

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--Footer Area End-->

        <!-- Modal -->
        <div class="modal flosun-modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <button type="button" class="close close-button" data-bs-dismiss="modal" aria-label="Close">
                        <span class="close-icon" aria-hidden="true">x</span>
                    </button>
                    <div class="modal-body">
                        <div class="container-fluid custom-area">
                            <div class="row">
                                <div class="col-md-6 col-custom">
                                    <div class="modal-product-img">
                                        <a class="w-100" href="#">
                                            <img class="w-100" src="http://127.0.0.1:8000/template/images/product/large-size/1.jpg" alt="Product">
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scroll to Top Start -->
        <a class="scroll-to-top show" href="#">
            <i class="lnr lnr-arrow-up"></i>
        </a>
        <!-- Scroll to Top End -->

        <!-- JS
    ============================================ -->

        <!-- jQuery JS -->
        <script src="http://127.0.0.1:8000/template/js/vendor/jquery-3.6.0.min.js"></script>
        <!-- jQuery Migrate JS -->
        <script src="http://127.0.0.1:8000/template/js/vendor/jquery-migrate-3.3.2.min.js"></script>
        <!-- Modernizer JS -->
        <script src="http://127.0.0.1:8000/template/js/vendor/modernizr-3.7.1.min.js"></script>
        <!-- Bootstrap JS -->
        <script src="http://127.0.0.1:8000/template/js/vendor/bootstrap.bundle.min.js"></script>

        <!-- Swiper Slider JS -->
        <script src="http://127.0.0.1:8000/template/js/plugins/swiper-bundle.min.js"></script>
        <!-- nice select JS -->
        <script src="http://127.0.0.1:8000/template/js/plugins/nice-select.min.js"></script>
        <!-- Ajaxchimpt js -->
        <script src="http://127.0.0.1:8000/template/js/plugins/jquery.ajaxchimp.min.js"></script>
        <!-- Jquery Ui js -->
        <script src="http://127.0.0.1:8000/template/js/plugins/jquery-ui.min.js"></script>
        <!-- Jquery Countdown js -->
        <script src="http://127.0.0.1:8000/template/js/plugins/jquery.countdown.min.js"></script>
        <!-- jquery magnific popup js -->
        <script src="http://127.0.0.1:8000/template/js/plugins/jquery.magnific-popup.min.js"></script>

        <!-- Main JS -->
        <script src="http://127.0.0.1:8000/template/js/main.js"></script>



        </body>

        <!-- Mirrored from htmldemo.net/flosun/flosun/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Dec 2022 05:03:14 GMT -->

        </html>