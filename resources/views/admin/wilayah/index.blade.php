@extends('layouts.default')
@section('kategori')

<body>
    <div class="container">
        @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif

        <h1>Daftar </h1>

        <table class="table table-bordered " style="text-align:center;">
            <thead style="background:#1572e8;">
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Deskripsi</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($wilayahs as $index => $wilayah)
                <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>{{ $wilayah->nama }}</td>
                    <td>{{ $wilayah->deskripsi }}</td>
                    <td>
                        <button type="button" class="btn btn-primary btn-sm " data-bs-toggle="modal" data-bs-target="#editModal{{ $wilayah->id }}">
                            <i class="fa-solid fa-pencil"></i>
                        </button>
                        <form action="{{ route('wilayah.destroy', $wilayah->id) }}" method="POST" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus kategori ini?')"><i class="fa-solid fa-trash"></i> </button>
                        </form>
                    </td>
                </tr>

                <!-- Modal Edit -->
                <div class="modal fade" id="editModal{{ $wilayah->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5" id="exampleModalLabel">Edit Kategori</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fa-solid fa-xmark"></i></button>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('wilayah.update', $wilayah->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group">
                                        <label for="nama">Nama Kategori:</label>
                                        <input type="text" class="form-control" name="nama" value="{{ $wilayah->nama }}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Deskripsi:</label>
                                        <textarea class="form-control" name="deskripsi" required>{{ $wilayah->deskripsi }}</textarea>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </tbody>
        </table>

        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Tambah Wilayah Baru
        </button>

        <!-- Modal Tambah Kategori -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Wilayah</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fa-solid fa-xmark"></i></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('wilayah.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="pulau">Nama pulau:</label>
                                <select class="form-control" name="pulau" id="pulau" required>
                                    <option value="">Pilih pulau</option>
                                    @foreach($pulau as $no => $data)
                                    <option value="{{ $data->nama_pulau }}" data-wilayah="{{ json_encode('$data->nama_pulau') }}">{{ $data->nama_pulau }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="nama">Nama Wilayah:</label>
                                <select class="form-control" name="nama" id="nama" required>
                                    <option value="">Pilih wilayah</option>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="deskripsi">Deskripsi:</label>
                                <textarea class="form-control" name="deskripsi" required></textarea>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup </button>
                                <button type="submit" class="btn btn-primary">Perubahan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

    <script>
        $(document).ready(function() {
            // Daftar wilayah di setiap pulau
            var wilayahData = {
                "Sumatera": [
                    "Pilih Kota",
                    "Medan",
                    "Pilih Kota",
                    "Padang",
                    "Pekanbaru",
                    "Palembang",
                    "Bandar Lampung",
                    "Bengkulu",
                    "Jambi",
                    "Tanjung Pinang",
                    "Bukittinggi",
                    "Payakumbuh",
                    "Dumai",
                    "Sibolga",
                    "Tebing Tinggi",
                    "Binjai",
                    "Lubuklinggau",
                    "Prabumulih",
                    "Metro",
                    "Sungai Penuh",
                    "Padang Sidempuan",
                    "Gunungsitoli"
                ],
                "Kalimantan": [
                    // Daftar kota di Kalimantan
                    "Pilih Kota",
                    "Pontianak",
                    "Banjarmasin",
                    "Samarinda",
                    "Palangkaraya",
                    "Balikpapan",
                    "Singkawang",
                    "Tarakan",
                    "Bontang",
                    // ...
                ],
                "Sulawesi": [
                    // Daftar kota di Sulawesi
                    "Pilih Kota",
                    "Makassar",
                    "Manado",
                    "Palu",
                    "Kendari",
                    "Gorontalo",
                    // ...
                ],
                "Jawa": [
                    // Daftar kota di Jawa
                    "Pilih Kota",
                    "Jakarta",
                    "Bandung",
                    "Surabaya",
                    "Semarang",
                    "Yogyakarta",
                    "Surakarta",
                    // ...
                ],
                "Papua": [
                    // Daftar kota di Papua
                    "Pilih Kota",
                    "Jayapura",
                    "Manokwari",
                    "Sorong",
                    "Biak",
                    "Merauke",
                    // ...
                ]
            };

            // Ketika dropdown pulau berubah
            $('#pulau').on('change', function() {
                var selectedPulau = $(this).find(':selected');
                var pulauValue = selectedPulau.val();

                // Bersihkan dropdown wilayah
                $('#nama').empty();

                // Tambahkan opsi wilayah yang sesuai dengan pulau yang dipilih
                if (wilayahData[pulauValue]) {
                    $.each(wilayahData[pulauValue], function(index, wilayah) {
                        $('#nama').append($('<option>', {
                            value: wilayah,
                            text: wilayah
                        }));
                    });
                } else {
                    // Jika pulau tidak dipilih atau pulau lain dipilih, tampilkan pesan default
                    $('#nama').append($('<option>', {
                        value: '',
                        text: 'Pilih wilayah'
                    }));
                }
            });
        });
    </script>



</body>

@endsection