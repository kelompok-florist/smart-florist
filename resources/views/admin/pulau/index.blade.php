@extends('layouts.default')
@section('pulau')

<body>
    <div class="container">
        @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif

        <h1>Daftar pulau</h1>

        <table class="table table-bordered " style="text-align:center;">
            <thead class="" style="background:#1572e8;">
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($pulaus as $index => $pulau)
                <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>{{ $pulau->nama_pulau }}</td>
                    <td>
                        <!-- <a href="{{ route('pulau.edit', $pulau->id) }}" class="btn btn-primary btn-sm">Edit</a> -->
                        <button type="button" class="btn btn-primary btn-sm " data-bs-toggle="modal" data-bs-target="#editModal{{ $pulau->id }}">
                            <i class="fa-solid fa-pencil"></i>
                        </button>
                        <form action="{{ route('pulau.destroy', $pulau->id) }}" method="POST" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus pulau ini?')"><i class="fa-solid fa-trash"></i> </button>
                        </form>
                    </td>
                </tr>

                <!-- Modal Edit -->
                <div class="modal fade" id="editModal{{ $pulau->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5" id="exampleModalLabel">Edit pulau</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fa-solid fa-xmark"></i></button>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('pulau.update', $pulau->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group">
                                        <label for="nama_pulau">Nama pulau:</label>
                                        <input type="text" class="form-control" name="nama_pulau" value="{{ $pulau->nama_pulau }}" required>
                                    </div>


                                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </tbody>
        </table>

        <!-- <a href="{{ route('pulau.create') }}" class="btn btn-primary">Tambah pulau Baru</a> -->
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Tambah pulau Baru
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Tambah pulau</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fa-solid fa-xmark"></i></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('pulau.store') }}" method="POST">
                            @csrf
                            <tr>
                                <div class="form-group">
                                    <label for="nama_pulau">Nama pulau:</label>
                                    <select class="form-control" name="nama_pulau" id="nama_pulau" required>
                                        <option value="">Pilih pulau</option>
                                        <option value="Sumatera">Sumatera</option>
                                        <option value="Jawa">Jawa</option>
                                        <option value="Sulawesi">Sulawesi</option>
                                        <option value="Kalimantan">Kalimantan</option>
                                    </select>
                                </div>

                            </tr>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

@endsection