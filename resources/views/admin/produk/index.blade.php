@extends('layouts.default')
@section('produk')

<body>
    <div class="container">
        @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif

        <h1>Daftar Produk</h1>

        <table class="table table-bordered" style="text-align:center;">
            <thead style="background:#1572e8;">
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Deskripsi</th>
                    <th>Kategori</th>
                    <th>Wilayah</th>
                    <th>Gambar</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($produks as $index => $produk)
                <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>{{ $produk->nama }}</td>
                    <td>Rp {{ number_format($produk->harga, 2) }}</td>
                    <td>{{ $produk->deskripsi }}</td>
                    <td>{{ $produk->kategori->nama }}</td>
                    <td>{{ $produk->wilayah->nama }}</td>
                    <td>
                        @if($produk->gambar)
                        <img src="{{ asset('storage/' . $produk->gambar) }}" alt="Gambar Produk" class="img-thumbnail" style="max-width: 100px;">
                        @else
                        Tidak Ada Gambar
                        @endif
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#editModal{{ $produk->id }}">
                            <i class="fa-solid fa-pencil"></i>
                        </button>
                        <form action="{{ route('produk.destroy' , $produk->id) }}" method="POST" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus produk ini?')">
                                <i class="fa-solid fa-trash"></i>
                            </button>
                        </form>
                    </td>
                </tr>

                <!-- Modal Edit -->
                <div class="modal fade" id="editModal{{ $produk->id }}" tabindex="-1" aria-labelledby="editModalLabel{{ $produk->id }}" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5" id="editModalLabel{{ $produk->id }}">Edit Produk</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fa-solid fa-xmark"></i></button>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('produk.update', $produk->id) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group">
                                        <label for="nama">Nama Produk:</label>
                                        <input type="text" class="form-control" name="nama" value="{{ $produk->nama }}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="harga">Harga:</label>
                                        <input type="number" class="form-control" name="harga" value="{{ $produk->harga }}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Deskripsi:</label>
                                        <textarea class="form-control" name="deskripsi" required>{{ $produk->deskripsi }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="gambar">Gambar:</label>
                                        @if($produk->gambar)
                                        <img src="{{ asset('storage/' . $produk->gambar) }}" alt="Gambar Produk" style="max-width: 100px;">
                                        @else
                                        Tidak Ada Gambar
                                        @endif
                                        <input type="file" class="form-control-file" name="gambar">
                                    </div>

                                    <div class="form-group">
                                        <label for="kategori_id">Kategori:</label>
                                        <select class="form-control" name="kategori_id" required>
                                            @foreach($kategoris as $kategori)
                                            <option value="{{ $kategori->id }}" {{ $produk->kategori_id == $kategori->id ? 'selected' : '' }}>{{ $kategori->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="wilayah_id">Wilayah:</label>
                                        <select class="form-control" name="wilayah_id" required>
                                            @foreach($wilayahs as $wilayah)
                                            <option value="{{ $wilayah->id }}">{{ $wilayah->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </tbody>
        </table>

        <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#addModal">
            Tambah Produk Baru
        </button>

        <!-- Modal Add -->
        <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="addModalLabel">Tambah Produk</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fa-solid fa-xmark"></i></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('produk.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="nama">Nama Produk:</label>
                                <input type="text" class="form-control" name="nama" required>
                            </div>

                            <div class="form-group">
                                <label for="harga">Harga:</label>
                                <input type="number" class="form-control" name="harga" required>
                            </div>

                            <div class="form-group">
                                <label for="deskripsi">Deskripsi:</label>
                                <textarea class="form-control" name="deskripsi" required></textarea>
                            </div>

                            <div class="form-group">
                                <label for="gambar">Gambar:</label>
                                <input type="file" class="form-control-file" name="gambar">
                            </div>

                            <div class="form-group">
                                <label for="kategori_id">Kategori:</label>
                                <select class="form-control" name="kategori_id" required>
                                    @foreach($kategoris as $kategori)
                                    <option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="wilayah_id">Wilayah:</label>
                                <select class="form-control" name="wilayah_id" required>
                                    @foreach($wilayahs as $wilayah)
                                    <option value="{{ $wilayah->id }}">{{ $wilayah->nama }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Tambah Produk</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

@endsection