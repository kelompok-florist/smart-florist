    @extends('layout.master')
    @section('content')

    <body>

        <aside class="off-canvas-wrapper" id="mobileMenu">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="btn-close-off-canvas">
                    <i class="fa fa-times"></i>
                </div>
                <div class="off-canvas-inner">
                    <div class="search-box-offcanvas">
                        <form>
                            <input type="text" placeholder="Search product..." />
                            <button class="search-btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <!-- mobile menu start -->
                    <div class="mobile-navigation">
                        <!-- mobile menu navigation start -->
                        <nav>
                            <ul class="mobile-menu">
                                <li class="menu-item-has-children">
                                    <a href="#">Home</a>
                                    <ul class="dropdown">
                                        <li><a href="index.html">Home Page 1</a></li>
                                        <li><a href="index-2.html">Home Page 2</a></li>
                                        <li><a href="index-3.html">Home Page 3</a></li>
                                        <li><a href="index-4.html">Home Page 4</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#">Shop</a>
                                    <ul class="megamenu dropdown">
                                        <li class="mega-title has-children">
                                            <a href="#">Shop Layouts</a>
                                            <ul class="dropdown">
                                                <li><a href="shop.html">Shop Left Sidebar</a></li>
                                                <li>
                                                    <a href="shop-right-sidebar.html">Shop Right Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="shop-list-left.html">Shop List Left Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="shop-list-right.html">Shop List Right Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="shop-fullwidth.html">Shop Full Width</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children">
                                            <a href="#">Product Details</a>
                                            <ul class="dropdown">
                                                <li>
                                                    <a href="product-details.html">Single Product Details</a>
                                                </li>
                                                <li>
                                                    <a href="variable-product-details.html">Variable Product Details</a>
                                                </li>
                                                <li>
                                                    <a href="external-product-details.html">External Product Details</a>
                                                </li>
                                                <li>
                                                    <a href="gallery-product-details.html">Gallery Product Details</a>
                                                </li>
                                                <li>
                                                    <a href="countdown-product-details.html">Countdown Product Details</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="mega-title has-children">
                                            <a href="#">Others</a>
                                            <ul class="dropdown">
                                                <li><a href="error404.html">Error 404</a></li>
                                                <li><a href="compare.html">Compare Page</a></li>
                                                <li><a href="cart.html">Cart Page</a></li>
                                                <li><a href="checkout.html">Checkout Page</a></li>
                                                <li><a href="wishlist.html">Wish List Page</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#">Blog</a>
                                    <ul class="dropdown">
                                        <li><a href="blog.html">Blog Left Sidebar</a></li>
                                        <li>
                                            <a href="blog-list-right-sidebar.html">Blog List Right Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="blog-list-fullwidth.html">Blog List Fullwidth</a>
                                        </li>
                                        <li><a href="blog-grid.html">Blog Grid Page</a></li>
                                        <li>
                                            <a href="blog-grid-right-sidebar.html">Blog Grid Right Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="blog-grid-fullwidth.html">Blog Grid Fullwidth</a>
                                        </li>
                                        <li>
                                            <a href="blog-details-sidebar.html">Blog Details Sidebar Page</a>
                                        </li>
                                        <li>
                                            <a href="blog-details-fullwidth.html">Blog Details Fullwidth Page</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#">Pages</a>
                                    <ul class="dropdown">
                                        <li><a href="my-account.html">My Account</a></li>
                                        <li>
                                            <a href="login-register.html">login &amp; register</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="about-us.html">About Us</a></li>
                                <li><a href="contact-us.html">Contact</a></li>
                            </ul>
                        </nav>
                        <!-- mobile menu navigation end -->
                    </div>
                    <!-- mobile menu end -->
                    <div class="offcanvas-widget-area">
                        <div class="switcher">
                            <div class="language">
                                <span class="switcher-title">Language: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">English</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">Indonesia</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="currency">
                                <span class="switcher-title">Currency: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">Rupiah</a>
                                            <ul class="switcher-dropdown">
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="top-info-wrap text-left text-black">
                            <ul class="address-info">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="info%40yourdomain.html">(1245) 2456 012</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <a href="info%40yourdomain.html">SmartFlorist@gmail.com</a>
                                </li>
                            </ul>
                            <div class="widget-social">
                                <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- off-canvas menu end -->
        <!-- off-canvas menu start -->
        <aside class="off-canvas-menu-wrapper" id="sideMenu">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="off-canvas-inner">
                    <div class="btn-close-off-canvas">
                        <i class="fa fa-times"></i>
                    </div>
                    <!-- offcanvas widget area start -->
                    <div class="offcanvas-widget-area">
                        <ul class="menu-top-menu">
                            <li><a href="about-us.html">About Us</a></li>
                        </ul>
                        <p class="desc-content">
                            Smart Florist adalah platform inovatif yang menghadirkan pengalaman belanja bunga yang cerdas dan efisien. Pengguna dapat dengan mudah menjelajahi berbagai jenis bunga dan rangkaian yang disesuaikan dengan berbagai perayaan atau keperluan khusus. Smart Florist tidak hanya menawarkan kemudahan dalam memilih dan memesan bunga secara online tetapi juga menyediakan fitur pintar, seperti rekomendasi rangkaian berdasarkan preferensi pengguna atau acara tertentu. Dengan teknologi canggih, Smart Florist mengubah cara orang berinteraksi dengan dunia bunga secara online, memberikan layanan yang tidak hanya praktis tetapi juga penuh makna.
                        </p>
                        <div class="switcher">
                            <div class="language">
                                <span class="switcher-title">Language: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">English</a>
                                            <ul class="switcher-dropdown">
                                                <li><a href="#">Indonesia</a></li>
                                                </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="currency">
                                <span class="switcher-title">Currency: </span>
                                <div class="switcher-menu">
                                    <ul>
                                        <li>
                                            <a href="#">Rupiah</a>
                                            <ul class="switcher-dropdown">
                                                </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="top-info-wrap text-left text-black">
                            <ul class="address-info">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="info%40yourdomain.html">(1245) 2456 012</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <a href="info%40yourdomain.html">SmartFlorist@gmail.com</a>
                                </li>
                            </ul>
                            <div class="widget-social">
                                <a class="facebook-color-bg" title="Facebook-f" href="#"><i class="fa fa-facebook-f"></i></a>
                                <a class="twitter-color-bg" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="linkedin-color-bg" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="youtube-color-bg" title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                                <a class="vimeo-color-bg" title="Vimeo" href="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- offcanvas widget area end -->
                </div>
            </div>
        </aside>
        <!-- off-canvas menu end -->
        </header>
        <!-- Header Area End Here -->
        <!-- Slider/Intro Section Start -->
        <div class="intro11-slider-wrap section">
            <div class="intro11-slider swiper-container">
                <div class="swiper-wrapper">
                    <div class="intro11-section swiper-slide slide-1 slide-bg-1 bg-position">
                        <!-- Intro Content Start -->
                        <div class="intro11-content text-left">
                            <h3 class="title-slider text-uppercase">Top Trend</h3>
                            <h2 class="title">2023 Flower Trend</h2>
                            <p class="desc-content">
                               Selamat datang di dunia kecantikan bunga, di mana setiap karangan bunga adalah karya seni yang menciptakan nuansa istimewa dalam setiap momen. Produk kami bukan hanya sekedar bunga, melainkan sentuhan elegan yang menghidupkan ruang dan merayakan kebahagiaan. Apresiasikan perasaanmu dengan produk terbaik kami.
                            </p>
                            <a href="product-details.html" class="btn flosun-button secondary-btn theme-color rounded-0">Shop Now</a>
                        </div>
                        <!-- Intro Content End -->
                    </div>
                    <div class="intro11-section swiper-slide slide-2 slide-bg-1 bg-position">
                        <!-- Intro Content Start -->
                        <div class="intro11-content text-left">
                            <h3 class="title-slider black-slider-title text-uppercase">
                                Collection
                            </h3>
                            <h2 class="title">
                                FlowersFloral Board  <br />
                                Papan Bunga
                            </h2>
                            <p class="desc-content">
                                Jelajahi koleksi eksklusif kami untuk menemukan karangan bunga yang mencerminkan perasaan Anda, karena di sini, setiap buket adalah ungkapan keindahan yang tak terlupakan.
                            </p>
                            <a href="product-details.html" class="btn flosun-button secondary-btn rounded-0">Shop Now</a>
                        </div>
                        <!-- Intro Content End -->
                    </div>
                </div>
                <!-- Slider Navigation -->
                <div class="home1-slider-prev swiper-button-prev main-slider-nav">
                    <i class="lnr lnr-arrow-left"></i>
                </div>
                <div class="home1-slider-next swiper-button-next main-slider-nav">
                    <i class="lnr lnr-arrow-right"></i>
                </div>
                <!-- Slider pagination -->
                <div class="swiper-pagination"></div>
            </div>
        </div>
        <!-- Slider/Intro Section End -->
        <!--Categories Area Start-->
        <div class="categories-area pt-40">
            <div class="container-fluid">
                <div class="row">
                    <div class="cat-1 col-md-4 col-sm-12 col-custom">
                        <div class="categories-img mb-30">
                            <a href="#"><img src="{{asset('template')}}/images/category/LLUMINALE 光を纏う森 _ ARCH DAYSエレガント スウィート 沖縄 _ WEDDING _ ARCH DAYS.jpg" alt="" /></a>
                            <div class="categories-content">
                                <h3>Potted Plant</h3>
                                <h4>18 items</h4>
                            </div>
                        </div>
                    </div>
                    <div class="cat-2 col-md-8 col-sm-12 col-custom">
                        <div class="row">
                            <div class="cat-3 col-md-7 col-custom">
                                <div class="categories-img mb-30">
                                    <a href="#"><img src="{{asset('template')}}/images/category/OIP (1).jpg" alt="" /></a>
                                    <div class="categories-content">
                                        <h3>Potted Plant</h3>
                                        <h4>18 items</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="cat-4 col-md-5 col-custom">
                                <div class="categories-img mb-30">
                                    <a href="#"><img src="{{asset('template')}}/images/category/OIP.jpg" alt="" /></a>
                                    <div class="categories-content">
                                        <h3>Potted Plant</h3>
                                        <h4>18 items</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="cat-5 col-md-4 col-custom">
                                <div class="categories-img mb-30">
                                    <a href="#"><img src="{{asset('template')}}/images/category/🤎_ ੈ✩‧₊˚.jpg" alt="" /></a>
                                    <div class="categories-content">
                                        <h3>Potted Plant</h3>
                                        <h4>18 items</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="cat-6 col-md-8 col-custom">
                                <div class="categories-img mb-30">
                                    <a href="#"><img src="{{asset('template')}}/images/category/home1-category-2.jpg" alt="" /></a>
                                    <div class="categories-content">
                                        <h3>Potted Plant</h3>
                                        <h4>18 items</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Categories Area End-->
        <!--Product Area Start-->
        <div class="product-area mt-text-2">
            <div class="container custom-area-2 overflow-hidden">
                <div class="row">
                    <!--Section Title Start-->
                    <div class="col-12 col-custom">
                        <div class="section-title text-center mb-30">
                            <span class="section-title-1">Wonderful gift</span>
                            <h3 class="section-title-3">Featured Products</h3>
                        </div>
                    </div>
                    <!--Section Title End-->
                </div>
                <div class="row product-row">
                    <div class="col-12 col-custom">
                        <div class="product-slider swiper-container anime-element-multi">
                            <div class="swiper-wrapper">
                                <div class="single-item swiper-slide">
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
                                                <img src="{{asset('template')}}/images/product/1-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                                </a>
                                            <span class="onsale">Sale!</span>
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                <a href="asset/img/1.jpg">Flowers daisy pink stick</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
<<<<<<< HEAD
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
=======
                                                <span class="regular-price">Rp=120.000</Rp></span>
                                                <span class="old-price"><del>Rp=150.000</del></span>
>>>>>>> Zoya
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/6-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                                </a>
=======
                                               <img src="{{asset('template')}}/images/cart/9__2_-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                            </a>
                                            </a>
>>>>>>> Zoya
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Jasmine flowers white</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
<<<<<<< HEAD
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
=======
                                                <span class="regular-price">Rp.80.000</span>
                                                <span class="old-price"><del>Rp.90.000</del></span>
>>>>>>> Zoya
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                </div>
                                <div class="single-item swiper-slide">
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/3-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                                </a>
=======
                                                <img src="{{asset('template')}}/images/cart/3-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                            </a>
>>>>>>> Zoya
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Blossom bouquet flower</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.120.000</span>
<<<<<<< HEAD
                                                <span class="old-price"><del>Rp.150.000</del></span>
=======
                                                <span class="old-price"><del>Rp.190.000</del></span>
>>>>>>> Zoya
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/4__2_-removebg-preview.png" alt="" class="product-image-1 w-100" />
=======
                                                <img src="{{asset('template')}}/images/cart/2-removebg-preview.png" alt="" class="product-image-1 w-100" />
>>>>>>> Zoya
                                            </a>
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Orchid flower red stick</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
<<<<<<< HEAD
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
=======
                                                <span class="regular-price">Rp.80.000</span>
                                                <span class="old-price"><del>Rp.120.000</del></span>
>>>>>>> Zoya
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                </div>
                                <div class="single-item swiper-slide">
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/2-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                                </a>
=======
                                                <img src="{{asset('template')}}/images/cart/9__1_-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                            </a>
>>>>>>> Zoya
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Rose bouquet white</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/9__2_-removebg-preview.png" alt="" class="product-image-1 w-100" />
=======
                                                <img src="{{asset('template')}}/images/cart/6-removebg-preview.png" alt="" class="product-image-1 w-100" />
>>>>>>> Zoya
                                               </a>
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Hyacinth white stick</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
<<<<<<< HEAD
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
=======
                                                <span class="regular-price">Rp.50.000</span>
                                                <span class="old-price"><del>Rp.90.000</del></span>
>>>>>>> Zoya
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                </div>
                                <div class="single-item swiper-slide">
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/8-removebg-preview.png" alt="" class="product-image-1 w-100" />
=======
                                                <img src="{{asset('template')}}/images/cart/8-removebg-preview.png" alt="" class="product-image-1 w-100" />
>>>>>>> Zoya
                                                </a>
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Glory of the Snow</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
<<<<<<< HEAD
                                                <span class="regular-price">Rp.120.000</span>
=======
                                                <span class="regular-price">Rp.100.000</span>
>>>>>>> Zoya
                                                <span class="old-price"><del>Rp.150.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/9__4_-removebg-preview.png" alt="" class="product-image-1 w-100" />
=======
                                                <img src="{{asset('template')}}/images/cart/7-removebg-preview.png" alt="" class="product-image-1 w-100" />
>>>>>>> Zoya
                                                </a>
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Jack in the Pulpit</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                </div>
                                <div class="single-item swiper-slide">
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/9__1_-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                                </a>
=======
                                                <img src="{{asset('template')}}/images/cart/Arrangements_Ikebana_67F-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                               </a>
>>>>>>> Zoya
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Pearly Everlasting</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/11.png" alt="" class="product-image-1 w-100" />
                                                </a>
=======
                                                <img src="{{asset('template')}}/images/cart/2-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                            </a>
>>>>>>> Zoya
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Flowers daisy pink stick</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                </div>
                                <div class="single-item swiper-slide">
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/7-removebg-preview.png" alt="" class="product-image-1 w-100" />
=======
                                                <img src="{{asset('template')}}/images/cart/3-removebg-preview.png" alt="" class="product-image-1 w-100" />
>>>>>>> Zoya
                                                </a>
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Flowers white</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/1-removebg-preview.png" alt="" class="product-image-1 w-100" />
=======
                                                <img src="{{asset('template')}}/images/cart/1-removebg-preview.png" alt="" class="product-image-1 w-100" />
>>>>>>> Zoya
                                                </a>
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Flower red stick</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                </div>
                            </div>
                            <!-- Slider pagination -->
                            <div class="swiper-pagination default-pagination"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Product Area End-->
        <!-- Product Countdown Area Start Here -->
        <div class="product-countdown-area mt-text-3">
            <div class="container custom-area">
                <div class="row">
                    <!--Section Title Start-->
                    <div class="col-12 col-custom">
                        <div class="section-title text-center mb-30">
                            <h3 class="section-title-3">Deal of The Day</h3>
                        </div>
                    </div>
                    <!--Section Title End-->
                </div>
                <div class="row">
                    <!--Countdown Start-->
                    <div class="col-12 col-custom">
                        <div class="countdown-area">
                            <div class="countdown-wrapper d-flex justify-content-center" data-countdown="2022/12/24"></div>
                        </div>
                    </div>
                    <!--Countdown End-->
                </div>
                <div class="row product-row">
                    <div class="col-12 col-custom">
                        <div class="item-carousel-2 swiper-container anime-element-multi product-area">
                            <div class="swiper-wrapper">
                                <div class="single-item swiper-slide">
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/12.jpg" alt="" class="product-image-1 w-100" />
                                                <img src="{{asset('template')}}/images/product/15.jpg" alt="" class="product-image-2 position-absolute w-100" />
                                            </a>
=======
                                                <img src="{{asset('template')}}/images/cart/ii-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                                </a>
>>>>>>> Zoya
                                            <span class="onsale">Sale!</span>
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Flowers pink stick</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                </div>
                                <div class="single-item swiper-slide">
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/16.jpg" alt="" class="product-image-1 w-100" />
                                                <img src="{{asset('template')}}/images/product/17.jpg" alt="" class="product-image-2 position-absolute w-100" />
=======
                                                <img src="{{asset('template')}}/images/cart/oo-removebg-preview.png" alt="" class="product-image-1 w-100" />
>>>>>>> Zoya
                                            </a>
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Flowers white</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                </div>
                                <div class="single-item swiper-slide">
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/15.jpg" alt="" class="product-image-1 w-100" />
                                                <img src="{{asset('template')}}/images/product/17.jpg" alt="" class="product-image-2 position-absolute w-100" />
                                            </a>
=======
                                                <img src="{{asset('template')}}/images/cart/A_flower_arrangement_to_please_the_eye_on_any_-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                               </a>
>>>>>>> Zoya
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Blossom flower</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                </div>
                                <div class="single-item swiper-slide">
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/17.jpg" alt="" class="product-image-1 w-100" />
                                                <img src="{{asset('template')}}/images/product/14.jpg" alt="" class="product-image-2 position-absolute w-100" />
                                            </a>
=======
                                                <img src="{{asset('template')}}/images/cart/gghhh-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                                </a>
>>>>>>> Zoya
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Flower red stick</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.140.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                </div>
                                <div class="single-item swiper-slide">
                                    <!--Single Product Start-->
                                    <div class="single-product position-relative mb-30">
                                        <div class="product-image">
                                            <a class="d-block" href="product-details.html">
<<<<<<< HEAD
                                                <img src="{{asset('template')}}/images/product/13.jpg" alt="" class="product-image-1 w-100" />
                                                <img src="{{asset('template')}}/images/product/14.jpg" alt="" class="product-image-2 position-absolute w-100" />
                                            </a>
=======
                                                <img src="{{asset('template')}}/images/cart/Pretty__pretty__pretty____-removebg-preview.png" alt="" class="product-image-1 w-100" />
                                                </a>
>>>>>>> Zoya
                                            <div class="add-action d-flex flex-column position-absolute">
                                                <a href="compare.html" title="Compare">
                                                    <i class="lnr lnr-sync" data-toggle="tooltip" data-placement="left" title="Compare"></i>
                                                </a>
                                                <a href="wishlist.html" title="Add To Wishlist">
                                                    <i class="lnr lnr-heart" data-toggle="tooltip" data-placement="left" title="Wishlist"></i>
                                                </a>
                                                <a href="#exampleModalCenter" title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                                    <i class="lnr lnr-eye" data-toggle="tooltip" data-placement="left" title="Quick View"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title-2">
                                                    <a href="product-details.html">Rose bouquet white</a>
                                                </h4>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">Rp.120.000</span>
                                                <span class="old-price"><del>Rp.150.000</del></span>
                                            </div>
                                            <a href="cart.html" class="btn product-cart">Add to Cart</a>
                                        </div>
                                    </div>
                                    <!--Single Product End-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Product Countdown Area End Here -->
        <!-- History Area Start Here -->
        <div class="our-history-area pt-text-3">
            <div class="container">
                <div class="row">
                    <!--Section Title Start-->
                    <div class="col-12">
                        <div class="section-title text-center mb-30">
                            <span class="section-title-1">Smart Florist</span>
                            <h2 class="section-title-large">Our History</h2>
                        </div>
                    </div>
                    <!--Section Title End-->
                </div>
                <div class="row">
                    <div class="col-lg-8 ms-auto me-auto">
                        <div class="history-area-content pb-0 mb-0 border-0 text-center">
                            <p>
<<<<<<< HEAD
                                <strong>Smart Florist adalah platform inovatif yang menghadirkan pengalaman belanja bunga yang cerdas dan efisien. Pengguna dapat dengan mudah menjelajahi berbagai jenis bunga dan rangkaian yang disesuaikan dengan berbagai perayaan atau keperluan khusus. Smart Florist tidak hanya menawarkan kemudahan dalam memilih dan memesan bunga secara online tetapi juga menyediakan fitur pintar, seperti rekomendasi rangkaian berdasarkan preferensi pengguna atau acara tertentu. Dengan teknologi canggih, Smart Florist mengubah cara orang berinteraksi dengan dunia bunga secara online, memberikan layanan yang tidak hanya praktis tetapi juga penuh makna.</strong>
                                </p>
=======
                                Selamat datang di Smart Florist: Smart Florist, surgawi bunga yang agennya terletak diseluruh indonesia. Ini adalah perjalanan yang luar biasa menjadi "Pengirim Pesan" cinta dan emosi kepada pelanggan tercinta.
                                Smart Florist memiliki koleksi buket, papan bunga,dan opsi hadiah lainnya. Apapun acaranya baik itu perayaan ulang tahun, wisuda, kejutan Valentine, pernikahan, pertunangan, kebahagiaan kelahiran, semangat Natal, Idul Fitri, dan lebih banyak lagi. Bunga adalah cara paling lugas dan berhasil untuk menyampaikan perasaan Anda kepada orang-orang yang Anda sayangi. Anda dapat menjadikan kami pilihan utama untuk mengungkapkan cinta Anda dalam setiap kesempatan, baik yang penuh sukacita maupun yang sedih. Hanya dengan satu klik, Smart Florist selalu siap dijangkau.
                                Biarkan Smart Florist menjadi bagian dari menciptakan momen kebahagiaan Anda.  Momen bahagia anda menjadi lebih istimewa ketika disertai dengan rangkaian bunga yang eksklusif dari Smart Florist.
                            </p>
>>>>>>> Zoya
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- History Area End Here -->
        <!-- Banner Area Start Here -->
        <div class="banner-area mt-text-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-custom">
                        <!--Single Banner Area Start-->
                        <div class="single-banner hover-style mb-30">
                            <div class="banner-img">
                                <a href="#">
                                <img src="{{asset('template')}}/images/banner/1-1.png" alt="" width="300" />
                                    <div class="overlay-1"></div>
                                </a>
                            </div>
                        </div>
                        <!--Single Banner Area End-->
                    </div>
                    <div class="col-md-4 col-custom">
                        <!--Single Banner Area Start-->
                        <div class="single-banner hover-style mb-30">
                            <div class="banner-img">
                                <a href="#">
                                    <img src="{{asset('template')}}/images/banner/11-2.png" alt="" />
                                    <div class="overlay-1"></div>
                                </a>
                            </div>
                        </div>
                        <!--Single Banner Area End-->
                    </div>
                    <div class="col-md-4 col-custom">
                        <!--Single Banner Area Start-->
                        <div class="single-banner hover-style mb-30">
                            <div class="banner-img">
                                <a href="#">
                                    <img src="{{asset('template')}}/images/banner/11-3.png" alt="" />
                                    <div class="overlay-1"></div>
                                </a>
                            </div>
                        </div>
                        <!--Single Banner Area End-->
                    </div>
                </div>
            </div>
        </div>
<<<<<<< HEAD
=======
        <!-- Banner Area End Here -->
        <!-- Testimonial Area Start Here -->
        <div class="testimonial-area mt-text-2">
            <div class="container custom-area">
                <div class="row">
                    <!--Section Title Start-->
                    <div class="col-12 col-custom">
                        <div class="section-title text-center">
                            <span class="section-title-1">Team work</span>
                        </div>
                    </div>
                    <!--Section Title End-->
                </div>
                <div class="row">
                    <div class="testimonial-carousel swiper-container intro11-carousel-wrap col-12 col-custom">
                        <div class="swiper-wrapper">
                            <div class="single-item swiper-slide">
                                <!--Single Testimonial Start-->
                                <div class="single-testimonial text-center">
                                    <div class="testimonial-img">
                                        <img src="{{asset('template')}}/images/testimonial/testimonial1.jpg" alt="" />
                                    </div>
                                    <div class="testimonial-content">
                                        <p>
                                            These guys have been absolutely outstanding. Perfect
                                            Themes and the best of all that you have many options to
                                            choose! Best Support team ever! Very fast responding!
                                            Thank you very much! I highly recommend this theme and
                                            these people!
                                        </p>
                                        <div class="testimonial-author">
                                            <h6>Katherine Sullivan <span>Customer</span></h6>
                                        </div>
                                    </div>
                                </div>
                                <!--Single Testimonial End-->
                            </div>
                            <div class="single-item swiper-slide">
                                <!--Single Testimonial Start-->
                                <div class="single-testimonial text-center">
                                    <div class="testimonial-img">
                                        <img src="{{asset('template')}}/images/testimonial/testimonial2.jpg" alt="" />
                                    </div>
                                    <div class="testimonial-content">
                                        <p>
                                            These guys have been absolutely outstanding. Perfect
                                            Themes and the best of all that you have many options to
                                            choose! Best Support team ever! Very fast responding!
                                            Thank you very much! I highly recommend this theme and
                                            these people!
                                        </p>
                                        <div class="testimonial-author">
                                            <h6>Alex Jhon <span>Customer</span></h6>
                                        </div>
                                    </div>
                                </div>
                                <!--Single Testimonial End-->
                            </div>
                        </div>
                        <!-- Slider Navigation -->
                        <div class="home1-slider-prev swiper-button-prev main-slider-nav">
                            <i class="lnr lnr-arrow-left"></i>
                        </div>
                        <div class="home1-slider-next swiper-button-next main-slider-nav">
                            <i class="lnr lnr-arrow-right"></i>
                        </div>
                        <!-- Slider pagination -->
                        <div class="swiper-pagination default-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonial Area End Here -->
        <!-- Newsletter Area Start Here -->
        <div class="news-letter-area gray-bg pt-no-text pb-no-text mt-text-3">
            <div class="container custom-area">
                <div class="row align-items-center">
                    <!--Section Title Start-->
                    <div class="col-md-6 col-custom">
                        <div class="section-title text-left mb-35">
                            <h3 class="section-title-3">Send Newsletter</h3>
                            <p class="desc-content mb-0">
                                Enter Your Email Address For Our Mailing List To Keep Your Self
                                Update
                            </p>
                        </div>
                    </div>
                    <!--Section Title End-->
                    <div class="col-md-6 col-custom">
                        <div class="news-latter-box">
                            <div class="newsletter-form-wrap text-center">
                                <form id="mc-form" class="mc-form">
                                    <input type="email" id="mc-email" class="form-control email-box" placeholder="email@example.com" name="EMAIL" />
                                    <button id="mc-submit" class="btn rounded-0" type="submit">
                                        Subscribe
                                    </button>
                                </form>
                                <!-- mailchimp-alerts Start -->
                                <div class="mailchimp-alerts text-centre">
                                    <div class="mailchimp-submitting"></div>
                                    <!-- mailchimp-submitting end -->
                                    <div class="mailchimp-success text-success"></div>
                                    <!-- mailchimp-success end -->
                                    <div class="mailchimp-error text-danger"></div>
                                    <!-- mailchimp-error end -->
                                </div>
                                <!-- mailchimp-alerts end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
>>>>>>> Zoya
        <!-- Newsletter Area End Here -->
        <!-- Blog Area Start Here -->
        <div class="blog-post-area mt-text-3">
            <div class="container custom-area">
                <div class="row">
                    <!--Section Title Start-->
                    <div class="col-12">
                        <div class="section-title text-center mb-30">
                            <h3 class="section-title-3">Jenis Product</h3>
                        </div>
                    </div>
                    <!--Section Title End-->
                </div>
                <div class="row">
                    <div class="col-12 col-md-4 col-lg-4 col-custom mb-30">
                        <div class="blog-lst">
                            <div class="single-blog">
                                <div class="blog-image">
                                    <a class="d-block" href="blog-details-fullwidth.html">
                                        <img src="{{asset('template')}}/images/blog/blog4.jpg" alt="Blog Image" class="w-100" />
                                    </a>
                                </div>
                                <div class="blog-content">
                                    <div class="blog-text">
                                        <h4>
                                            <a href="blog-details-fullwidth.html">Papan Bunga</a>
                                        </h4>
                                        <div class="blog-post-info">
                                            <span><a href="#">By admin</a></span>
                                            <span>December 18, 2022</span>
                                        </div>
                                        <p>
                                            papan ini menjadi ekspresi tulus dalam menyampaikan ucapan selamat, harapan, atau dukacita. Dengan paduan bunga-bunga yang dipilih secara hati-hati, papan ini menjadi karya seni hidup yang menggambarkan kehangatan dan perhatian. Setiap rangkaian bunga dipilih dengan teliti, menciptakan kombinasi warna dan tekstur yang memukau. Dari acara pernikahan yang meriah hingga momen duka yang penuh penghormatan, papan ucapan bunga menjadi simbol emosional yang mengabadikan pesan-pesan yang tak terlupakan.
                                        </p>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4 col-custom mb-30">
                        <div class="blog-lst">
                            <div class="single-blog">
                                <div class="blog-image">
                                    <a class="d-block" href="blog-details-fullwidth.html">
                                        <img src="{{asset('template')}}/images/blog/blog5.jpg" alt="Blog Image" class="w-100" />
                                    </a>
                                </div>
                                <div class="blog-content">
                                    <div class="blog-text">
                                        <h4>
                                            <a href="blog-details-fullwidth.html">Karangan Bunga</a>
                                        </h4>
                                        <div class="blog-post-info">
                                            <span><a href="#">By admin</a></span>
                                            <span>December 18, 2022</span>
                                        </div>
                                        <p>
                                            Kombinasi bunga yang dipilih memberikan pesan yang mendalam, mewakili keindahan di setiap langkah perjalanan. Sebagai ungkapan cinta, kasih sayang, atau dukungan, karangan bunga menjadi bahasa bunga yang tak terucapkan. Dengan aroma harum dan keelokan warna, setiap karangan bunga adalah lukisan hidup yang menyinari momen-momen istimewa. 
                                        </p>
                                       </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4 col-custom mb-30">
                        <div class="blog-lst">
                            <div class="single-blog">
                                <div class="blog-image">
                                    <a class="d-block" href="blog-details-fullwidth.html">
                                        <img src="{{asset('template')}}/images/blog/blog6.jpg" alt="Blog Image" class="w-100" />
                                    </a>
                                </div>
                                <div class="blog-content">
                                    <div class="blog-text">
                                        <h4>
                                            <a href="blog-details-fullwidth.html">Bucket Bunga</a>
                                        </h4>
                                        <div class="blog-post-info">
                                            <span><a href="#">By admin</a></span>
                                            <span>December 18, 2022</span>
                                        </div>
                                        <p>
                                            Setiap kelopak bunga yang dipilih secara seksama adalah catatan kehidupan yang merayakan keindahan dan keberagaman. Dari keceriaan rangkaian warna hingga harum yang memikat, setiap bucket bunga adalah simbol kegembiraan yang dihadirkan dalam sebuah rangkaian indah, memberikan sentuhan tulus pada setiap peristiwa.
                                        </p>
                                       </div>
                                       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Blog Area End Here -->
        <!-- Brand Logo Area Start Here -->
        <div class="brand-logo-area gray-bg pt-no-text pb-no-text mt-text-5">
            <div class="container custom-area">
                <div class="row">
                    <div class="col-12 col-custom">
                        <div class="brand-logo-carousel swiper-container intro11-carousel-wrap arrow-style-3">
                            <div class="swiper-wrapper">
                                <div class="single-brand swiper-slide">
                                    <a href="#">
                                        <img src="{{asset('template')}}/images/brand/1.png" alt="Brand Logo" />
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide">
                                    <a href="#">
                                        <img src="{{asset('template')}}/images/brand/2.png" alt="Brand Logo" />
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide">
                                    <a href="#">
                                        <img src="{{asset('template')}}/images/brand/3.png" alt="Brand Logo" />
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide">
                                    <a href="#">
                                        <img src="{{asset('template')}}/images/brand/4.png" alt="Brand Logo" />
                                    </a>
                                </div>
                                <div class="single-brand swiper-slide">
                                    <a href="#">
                                        <img src="{{asset('template')}}/images/brand/5.png" alt="Brand Logo" />
                                    </a>
                                </div>
                            </div>
                            <!-- Slider Navigation -->
                            <div class="home1-slider-prev swiper-button-prev main-slider-nav">
                                <i class="lnr lnr-arrow-left"></i>
                            </div>
                            <div class="home1-slider-next swiper-button-next main-slider-nav">
                                <i class="lnr lnr-arrow-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Brand Logo Area End Here -->


        <!-- Modal -->
        <div class="modal flosun-modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <button type="button" class="close close-button" data-bs-dismiss="modal" aria-label="Close">
                        <span class="close-icon" aria-hidden="true">x</span>
                    </button>
                    <div class="modal-body">
                        <div class="container-fluid custom-area">
                            <div class="row">
                                <div class="col-md-6 col-custom">
                                    <div class="modal-product-img">
                                        <a class="w-100" href="#">
                                            <img class="w-100" src="{{asset('template')}}/images/product/large-size/1.jpg" alt="Product" />
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-custom">
                                    <div class="modal-product">
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title">Product dummy name</h4>
                                            </div>
                                            <div class="price-box">
                                                <span class="regular-price">$80.00</span>
                                                <span class="old-price"><del>$90.00</del></span>
                                            </div>
                                            <div class="product-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <span>1 Review</span>
                                            </div>
                                            <p class="desc-content">
                                                we denounce with righteous indignation and dislike men
                                                who are so beguiled and demoralized by the charms of
                                                pleasure of the moment, so blinded by desire, that they
                                                cannot foresee the pain and trouble that are bound to
                                                ensue; and equal blame bel...
                                            </p>
                                            <form class="d-flex flex-column w-100" action="#">
                                                <div class="form-group">
                                                    <select class="form-control nice-select w-100">
                                                        <option>S</option>
                                                        <option>M</option>
                                                        <option>L</option>
                                                        <option>XL</option>
                                                        <option>XXL</option>
                                                    </select>
                                                </div>
                                            </form>
                                            <div class="quantity-with-btn">
                                                <div class="quantity">
                                                    <div class="cart-plus-minus">
                                                        <input class="cart-plus-minus-box" value="0" type="text" />
                                                        <div class="dec qtybutton">-</div>
                                                        <div class="inc qtybutton">+</div>
                                                        <div class="dec qtybutton">
                                                            <i class="fa fa-minus"></i>
                                                        </div>
                                                        <div class="inc qtybutton">
                                                            <i class="fa fa-plus"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="add-to_btn">
                                                    <a class="btn product-cart button-icon flosun-button dark-btn" href="cart.html">Add to cart</a>
                                                    <a class="btn flosun-button secondary-btn rounded-0" href="wishlist.html">Add to wishlist</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scroll to Top Start -->
        <a class="scroll-to-top" href="#">
            <i class="lnr lnr-arrow-up"></i>
        </a>
        @endsection