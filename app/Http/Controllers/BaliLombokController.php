<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaliLombokController extends Controller
{
    //Bali
    public function Bali()
    {
        return view('Bali&Lombok.Bali');
    }
    //Denpasar
    public function Denpasar()
    {
        return view('Bali&Lombok.Denpasar');
    }
    //Lombok
    public function Lombok()
    {
        return view('Bali&Lombok.Lombok');
    }
    //Mataram
    public function Mataram()
    {
        return view('Bali&Lombok.Mataram');
    }
}
