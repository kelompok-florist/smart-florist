<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KalimantanController extends Controller
{
    //Pontianak
    public function Pontianak()
    {
        return view('Kalimantan.Pontianak');
    }
    //Palangkaraya
    public function Palangkaraya()
    {
        return view('Kalimantan.Palangkaraya');
    }
    //Samarinda
    public function Samarinda()
    {
        return view('Kalimantan.Samarinda');
    }
    //Banjarmasin
    public function Banjarmasin()
    {
        return view('Kalimantan.Banjarmasin');
    }
    //Balikpapan
    public function Balikpapan()
    {
        return view('Kalimantan.Balikpapan');
    }
}
