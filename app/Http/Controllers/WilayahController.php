<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Wilayah;
use App\Models\Pulau;

class WilayahController extends Controller
{
    // Menampilkan daftar wilayah
    public function index()
    {
        $wilayahs = Wilayah::all();
        $pulau = Pulau::all();
        return view('admin.wilayah.index', compact('wilayahs', 'pulau'));
    }

    // Menampilkan formulir tambah wilayah
    public function create()
    {
        return view('admin.wilayah.create');
    }

    // Menyimpan wilayah baru
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string',
            'deskripsi' => 'required|string',
        ]);

        Wilayah::create($request->all());

        return redirect()->route('wilayah.index')->with('success', 'Wilayah berhasil ditambahkan.');
    }

    // Menampilkan formulir edit wilayah
    public function edit($id)
    {
        $wilayah = Wilayah::findOrFail($id);
        return view('admin.wilayah.edit', compact('wilayah'));
    }

    // Menyimpan perubahan pada wilayah
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|string',
            'deskripsi' => 'required|string',
        ]);

        $wilayah = Wilayah::findOrFail($id);
        $wilayah->update($request->all());

        return redirect()->route('wilayah.index')->with('success', 'Wilayah berhasil diperbarui.');
    }

    // Menghapus wilayah
    public function destroy($id)
    {
        $wilayah = Wilayah::findOrFail($id);
        $wilayah->delete();

        return redirect()->route('wilayah.index')->with('success', 'Wilayah berhasil dihapus.');
    }
}
