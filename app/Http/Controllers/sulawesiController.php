<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SulawesiController extends Controller
{
    //Makasar
    public function Makasar()
    {
        return view('Sulawesi.Makasar');
    }
    //Palu
    public function palu()
    {
        return view('Sulawesi.Palu');
    }
    //Kendari
    public function Kendari()
    {
        return view('Sulawesi.Kendari');
    }
    //Gorontalo
    public function Gorontalo()
    {
        return view('Sulawesi.Gorontalo');
    }
    //Manado
    public function Manado()
    {
        return view('Sulawesi.Manado');
    }
}
