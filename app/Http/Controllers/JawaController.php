<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JawaController extends Controller
{
    //Bandung
    public function Bandung()
    {
        return view('Jawa.Bandung');
    }
    //Banyumas
    public function Banyumas()
    {
        return view('Jawa.Banyumas');
    }
    //Cianjur
    public function Cianjur()
    {
        return view('Jawa.Cianjur');
    }
    //Cirebon
    public function Cirebon()
    {
        return view('Jawa.Cirebon');
    }
    //Garut
    public function Garut()
    {
        return view('Jawa.Garut');
    }
    //Kebumen
    public function Kebumen()
    {
        return view('Jawa.Kebumen');
    }
    //Kediri
    public function Kediri()
    {
        return view('Jawa.Kediri');
    }
    //Kudus
    public function Kudus()
    {
        return view('Jawa.Kudus');
    }
    //Magelang
    public function Magelang()
    {
        return view('Jawa.Magelang');
    }
    //Malang
    public function Malang()
    {
        return view('Jawa.Malang');
    }
    //Pekalongan
    public function Pekalongan()
    {
        return view('Jawa.Pekalongan');
    }
    //Purbalingga
    public function Purbalingga()
    {
        return view('Jawa.Purbalingga');
    }
    //Purwokerto
    public function Purwokerto()
    {
        return view('Jawa.Purwokerto');
    }
    //Rempang
    public function Rempang()
    {
        return view('Jawa.Rempang');
    }
    //Semarang
    public function Semarang()
    {
        return view('Jawa.Semarang');
    }
    //Solo
    public function Solo()
    {
        return view('Jawa.Solo');
    }
    //Sukabumi
    public function Sukabumi()
    {
        return view('Jawa.Sukabumi');
    }
    //Surabaya
    public function Surabaya()
    {
        return view('Jawa.Surabaya');
    }
    //Tasikmalaya
    public function Tasikmalaya()
    {
        return view('Jawa.Tasikmalaya');
    }
    //Tegal
    public function Tegal()
    {
        return view('Jawa.Tegal');
    }
    //Wonogiri
    public function Wonogiri()
    {
        return view('Jawa.Wonogiri');
    }
    //Wonosobo
    public function Wonosobo()
    {
        return view('Jawa.Wonosobo');
    }
    //Yogyakarta
    public function Yogyakarta()
    {
        return view('Jawa.Yogyakarta');
    }
}
