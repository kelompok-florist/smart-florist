<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\Request;
use App\Models\Wilayah;

class SumateraController extends Controller
{
    //Aceh
    public function Aceh()
    {
        $produk = Produk::all();
        $kode = '1';
        return view('Sumatera.Aceh', compact('produk', 'kode'));
    }
    //Bangka
    public function Bangka()
    {
        return view('Sumatera.Bangka');
    }
    //Batam
    public function Batam()
    {
        return view('Sumatera.Batam');
    }
    //Batu Raja
    public function BatuRaja()
    {
        return view('Sumatera.Batu Raja');
    }
    //Bengkulu
    public function Bengkulu()
    {
        return view('Sumatera.Bengkulu');
    }
    //Bukit Tinggi
    public function BukitTinggi()
    {
        return view('Sumatera.Bukit Tinggi');
    }
    //Jambi
    public function Jambi()
    {
        return view('Sumatera.JAMBI');
    }
    //Kotabumi
    public function Kotabumi()
    {
        return view('Sumatera.Kotabumi');
    }
    //Lampung
    public function Lampung()
    {
        return view('Sumatera.Lampung');
    }
    //Medan
    public function Medan()
    {
        return view('Sumatera.Medan');
    }
    //Muara Bulian
    public function MuaraBulian()
    {
        return view('Sumatera.Muara Bulian');
    }
    //Padang
    public function Padang()
    {
        return view('Sumatera.Padang');
    }
    //Pekanbaru
    public function Pekanbaru()
    {
        return view('Sumatera.Pekanbaru');
    }
    //Palembang
    public function Palembang()
    {
        return view('Sumatera.Palembang');
    }
    //Pematang Siantar
    public function PematangSiantar()
    {
        return view('Sumatera.Pematang Siantar');
    }
    //Riau
    public function Riau()
    {
        return view('Sumatera.Riau');
    }
    //Tanjung Pinang
    public function TanjungPinang()
    {
        return view('Sumatera.Tanjung Pinang');
    }
}
