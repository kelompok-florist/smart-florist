<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pulau;

class PulauController extends Controller
{
    // Menampilkan daftar Pulau
    public function index()
    {
        $pulaus = Pulau::all();
        return view('admin.pulau.index', ['pulaus' => $pulaus]);
    }

    // Menampilkan formulir tambah pulau
    public function create()
    {
        return view('admin.pulau.create');
    }

    // Menyimpan pulau baru
    public function store(Request $request)
    {
        $request->validate([
            'nama_pulau' => 'required|string',

        ]);

        Pulau::create($request->all());

        return redirect()->route('pulau.index')->with('success', 'pulau berhasil ditambahkan.');
    }

    // Menampilkan formulir edit pulau
    public function edit($id)
    {
        $pulau = Pulau::findOrFail($id);
        return view('admin.pulau.edit', compact('pulau'));
    }

    // Menyimpan perubahan pada pulau
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_pulau' => 'required|string',

        ]);

        $pulau = Pulau::findOrFail($id);
        $pulau->update($request->all());

        return redirect()->route('pulau.index')->with('success', 'pulau berhasil diperbarui.');
    }

    // Menghapus pulau
    public function destroy($id)
    {
        $pulau = Pulau::findOrFail($id);
        $pulau->delete();

        return redirect()->route('pulau.index')->with('success', 'pulau berhasil dihapus.');
    }
}
