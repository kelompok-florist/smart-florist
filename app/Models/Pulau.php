<?php
// App\Models\Provinsi.php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pulau extends Model
{
    protected $table = 'pulaus';
    protected $fillable = ['nama_pulau'];
}
