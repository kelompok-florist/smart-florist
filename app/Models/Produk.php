<?php


namespace App\Models;

use App\Http\Controllers\ProdukController;
use App\Http\Controllers\WilayahController;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $fillable = ['nama', 'harga', 'deskripsi', 'gambar', 'kategori_id', 'wilayah_id'];

    public function kategori()
    {
        return $this->belongsTo(Kategori::class);
    }
    public function wilayah()
    {
        return $this->belongsTo(Wilayah::class);
    }
}
